package net.scalytica.gitlab4s.client

import java.time.ZonedDateTime

import cats.effect.IO
import cats.syntax.either._
import io.circe._
import io.circe.generic.extras.Configuration
import io.circe.jackson._
import net.scalytica.gitlab4s.client.APIVersions.APIVersion
import net.scalytica.gitlab4s.client.GitLabClient._
import net.scalytica.gitlab4s.client.GitLabResponseHeaders._
import org.http4s.MediaType._
import org.http4s.client.blaze._
import org.http4s.dsl.io._
import org.http4s.headers._
import org.http4s.util.CaseInsensitiveString
import org.http4s.{EmptyBody, Header, Request, Response, Uri, UrlForm}
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

class GitLabClient private (
    gitlabHost: GitLabHost,
    accessToken: AccessToken,
    projectUri: ProjectUri,
    apiVersion: APIVersion
) {

  private[this] val logger = LoggerFactory.getLogger(classOf[GitLabClient])

  private[this] val TokenHeaderKey = "PRIVATE-TOKEN"

  private[this] val ReqHeaders = Seq(
    Accept(`application/json`),
    `Content-Type`(`application/json`),
    Header(TokenHeaderKey, accessToken.value)
  )

  private[this] val http4sClient =
    Http1Client[IO](
      BlazeClientConfig.defaultConfig.copy(
        requestTimeout = 5 minutes,
        responseHeaderTimeout = 5 minutes
      )
    )

  implicit val snakeCaseConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames

  implicit val encodeInstant: Encoder[ZonedDateTime] =
    Encoder.encodeString.contramap[ZonedDateTime](_.toString)

  implicit val decodeInstant: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { str =>
      Either
        .catchNonFatal(ZonedDateTime.parse(str))
        .leftMap(t => s"ZonedDateTime could not be parsed: ${t.getMessage}")
    }

  private[this] val LinkHeaderKey = CaseInsensitiveString(Link)

  private[this] def nextUrl(response: Response[IO]): Option[GitLabUrl] =
    parseLinkHeader(response).get("next").map(GitLabUrl.apply)

  private[this] def parseLinkHeader(resp: Response[IO]): Map[String, String] = {
    resp.headers
      .get(LinkHeaderKey)
      .map { header =>
        val linkLines = header.value.split(",").map(_.trim)
        linkLines.map { ll =>
          val splat   = ll.splitAt(ll.indexOf(";"))
          val linkStr = splat._1.stripPrefix("<").stripSuffix(">")
          val key     = splat._2.stripPrefix("""; rel="""").stripSuffix("\"")
          key -> linkStr
        }.toMap
      }
      .getOrElse(Map.empty)
  }

  private[this] def getRequest(gitlabUrl: GitLabUrl): Request[IO] = {
    logger.trace(s"Building GET request with URL: ${gitlabUrl.url}")
    GET(Uri.unsafeFromString(gitlabUrl.url), ReqHeaders: _*)
  }

  private[this] def postRequest(
      gitlabUrl: GitLabUrl,
      body: Option[Json]
  ): Request[IO] = {
    logger.trace(
      s"Building POST request with URL: ${gitlabUrl.url} and" +
        s"JSON body:\n${body.toString}"
    )
    POST(
      Uri.unsafeFromString(gitlabUrl.url),
      body.map(_).getOrElse(EmptyBody),
      ReqHeaders: _*
    )
  }

  private[this] def postFormRequest(
      gitlabUrl: GitLabUrl,
      formParams: UrlForm
  ): Request[IO] = {
    logger.trace(
      s"Building POST request with URL: ${gitlabUrl.url} and" +
        s"FORM body:\n${formParams.toString}"
    )
    POST(Uri.unsafeFromString(gitlabUrl.url), formParams, ReqHeaders: _*)
  }

  private[this] def putRequest(
      gitlabUrl: GitLabUrl,
      body: Option[Json]
  ): Request[IO] = {
    logger.trace(
      s"Building PUT request with URL: ${gitlabUrl.url} and" +
        s" body:\n${body.toString}"
    )
    PUT(
      Uri.unsafeFromString(gitlabUrl.url),
      body.map(_).getOrElse(EmptyBody),
      ReqHeaders: _*
    )
  }

  private[this] def deleteRequest(gitlabUrl: GitLabUrl): Request[IO] = {
    logger.trace(s"Building DELETE request with URL: ${gitlabUrl.url}")
    DELETE(
      Uri.unsafeFromString(gitlabUrl.url),
      ReqHeaders: _*
    )
  }

  def all[T](
      gitlabUrl: GitLabUrl
  )(implicit decoder: Decoder[Seq[T]]): IO[Seq[T]] = {
    def next(
        optLink: Option[GitLabUrl],
        stateIO: IO[Seq[T]] = IO.pure(Seq.empty[T])
    ): IO[Seq[T]] = optLink match {
      case None =>
        logger.debug("No more pages...processing complete")
        stateIO

      case Some(link) =>
        http4sClient.flatMap { client =>
          client.fetch(getRequest(link)) {
            case response if response.status.isSuccess =>
              val resIO =
                response
                  .as[String]
                  .flatMap(js => IO.fromEither(decode[Seq[T]](js)))
                  .flatMap(t => stateIO.map(_ ++: t))

              val nextUrlLink = nextUrl(response)
              logger.trace(s"next url is: $nextUrlLink")
              next(nextUrlLink, resIO)

            case response =>
              IO.raiseError(GitLabError(link, response.status))
          }
        }
    }

    next(Some(gitlabUrl))
  }

  def paged[T](
      gitlabUrl: GitLabUrl,
      page: Int = StartPage,
      perPage: Int = DefaultPerPage
  )(implicit decoder: Decoder[Seq[T]]): IO[Seq[T]] = {
    http4sClient.flatMap { client =>
      client.fetch(getRequest(gitlabUrl.withPage(page).withPerPage(perPage))) {
        case response if response.status.isSuccess =>
          response.as[String].flatMap(js => IO.fromEither(decode[Seq[T]](js)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def get[T](
      gitlabUrl: GitLabUrl
  )(implicit decoder: Decoder[T]): IO[Option[T]] = {
    http4sClient.flatMap { client =>
      client.fetch(getRequest(gitlabUrl)) {
        case response if response.status.isSuccess =>
          response
            .as[String]
            .flatMap(js => IO.fromEither(decode[T](js).map(Option.apply)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def post[T, R](
      gitlabUrl: GitLabUrl,
      data: T
  )(implicit encoder: Encoder[T], decoder: Decoder[R]): IO[R] = {
    http4sClient.flatMap { client =>
      client.fetch(postRequest(gitlabUrl, Some(encoder(data)))) {
        case response if response.status.isSuccess =>
          response.as[String].flatMap(js => IO.fromEither(decode[R](js)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def postForm[R](
      gitlabUrl: GitLabUrl,
      formData: Map[String, String]
  )(implicit decoder: Decoder[R]): IO[R] = {
    http4sClient.flatMap { client =>
      val form = UrlForm(formData.toSeq: _*)
      client.fetch(postFormRequest(gitlabUrl, form)) {
        case response if response.status.isSuccess =>
          response.as[String].flatMap(js => IO.fromEither(decode[R](js)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def postNoBody[R](
      gitlabUrl: GitLabUrl
  )(implicit decoder: Decoder[R]): IO[R] = {
    http4sClient.flatMap { client =>
      client.fetch(postRequest(gitlabUrl, None)) {
        case response if response.status.isSuccess =>
          response.as[String].flatMap(js => IO.fromEither(decode[R](js)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def put[T, R](
      gitlabUrl: GitLabUrl,
      data: Option[T]
  )(implicit encoder: Encoder[T], decoder: Decoder[R]): IO[Option[R]] = {
    http4sClient.flatMap { client =>
      client.fetch(putRequest(gitlabUrl, data.map(encoder.apply))) {
        case response if response.status.isSuccess =>
          response
            .as[String]
            .flatMap(js => IO.fromEither(decode[R](js).map(Option.apply)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def putNoBody[R](
      gitlabUrl: GitLabUrl
  )(implicit decoder: Decoder[R]): IO[R] = {
    http4sClient.flatMap { client =>
      client.fetch(putRequest(gitlabUrl, None)) {
        case response if response.status.isSuccess =>
          response.as[String].flatMap(js => IO.fromEither(decode[R](js)))

        case response =>
          IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def delete(gitlabUrl: GitLabUrl): IO[Unit] = {
    http4sClient.flatMap { client =>
      client.expectOr(deleteRequest(gitlabUrl)) { response =>
        IO.raiseError(GitLabError(gitlabUrl, response.status))
      }
    }
  }

  def close: IO[Unit] = http4sClient.flatMap(_.shutdown)

}

object GitLabClient {

  val StartPage: Int      = 1
  val DefaultPerPage: Int = 10

  def apply(
      gitlabHost: GitLabHost,
      accessToken: AccessToken,
      projectPath: ProjectUri,
      apiVersion: APIVersion = APIVersions.V4
  ): GitLabClient =
    new GitLabClient(gitlabHost, accessToken, projectPath, apiVersion)

}
