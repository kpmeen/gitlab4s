package net.scalytica.gitlab4s.client

case class GitLabHost(host: String = "gitlab.com", useTLS: Boolean = true) {

  def url: String = s"${if (useTLS) "https" else "http"}://$host"

}
