package net.scalytica.gitlab4s.client

case class AccessToken(value: String) {
  require(Option(value).nonEmpty)
}
