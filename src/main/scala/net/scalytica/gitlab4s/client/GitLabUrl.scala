package net.scalytica.gitlab4s.client
import GitLabUrl.{PageParam, PerPageParam}
case class GitLabUrl(
    value: String,
    queryParams: Map[String, String] = Map.empty
) {

  def withPage(pageNum: Int): GitLabUrl = {
    copy(queryParams = queryParams ++ Map(PageParam -> pageNum.toString))
  }

  def withPerPage(perPage: Int): GitLabUrl = {
    copy(queryParams = queryParams ++ Map(PerPageParam -> perPage.toString))
  }

  def append(path: String): GitLabUrl = {
    copy(s"${value.stripSuffix("/")}/${path.stripPrefix("/")}")
  }

  def append(path: Int): GitLabUrl = append(path.toString)

  def withQueryParams(params: (String, String)*): GitLabUrl =
    copy(queryParams = params.toMap)

  def url: String =
    s"$value?${queryParams.map(kv => s"${kv._1}=${kv._2}").mkString("&")}"

}

object GitLabUrl {

  val PageParam    = "page"
  val PerPageParam = "per_page"

  def apply(str: String): GitLabUrl = {
    val (url, queryStr) = str.splitAt(str.indexOf("?") + 1)
    val params = queryStr
      .split("&")
      .map(s => s.splitAt(s.indexOf("=")))
      .toMap
      .mapValues(_.stripPrefix("="))

    GitLabUrl(url.stripSuffix("?"), params)
  }
}
