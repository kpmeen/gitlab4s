package net.scalytica.gitlab4s.client

import org.http4s.Status

import scala.util.control.NoStackTrace

case class GitLabError(
    url: GitLabUrl,
    status: Status
) extends Exception
    with NoStackTrace {

  override def toString: String =
    s"GitLabError[url:${url.url} status:${status.toString}]"

}
