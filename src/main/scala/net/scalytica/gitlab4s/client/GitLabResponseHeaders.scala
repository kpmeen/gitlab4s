package net.scalytica.gitlab4s.client

object GitLabResponseHeaders {

  val Link        = "Link"
  val XTotal      = "X-Total"
  val XTotalPages = "X-Total-Pages"
  val XPerPage    = "X-Per-Page"
  val XPage       = "X-Page"
  val XNextPage   = "X-Next-Page"
  val XPrevPage   = "X-Prev-Page"

}
