package net.scalytica.gitlab4s.client

import net.scalytica.gitlab4s.client.APIVersions.APIVersion

case class GitLabContext(
    gitlabHost: GitLabHost,
    projectUri: ProjectUri,
    apiVersion: APIVersion,
    accessToken: AccessToken
)
