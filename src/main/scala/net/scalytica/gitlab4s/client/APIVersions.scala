package net.scalytica.gitlab4s.client

/**
 * Contains an ADT defining the supported GitLab versions.
 *
 * Currently, the gitlab4s library supports the following versions:
 *
 * <ul>
 *   <li>v4</li>
 * </ul>
 *
 * There are no plans to support older versions than v4 as GitLab has plans to
 * implement newer versions of the APIs using GraphQL. Any additional or
 * modified services to the JSON API will be built as a skeleton over these.
 */
object APIVersions {

  sealed abstract class APIVersion(val version: String) {
    override def toString: String = version
  }

  case object V4 extends APIVersion("v4")

}
