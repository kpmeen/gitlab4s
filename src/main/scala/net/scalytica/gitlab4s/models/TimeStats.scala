package net.scalytica.gitlab4s.models

/*
"time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0
    "human_time_estimate": null,
    "human_total_time_spent": null,
}
 */

case class TimeStats(
    timeEstimate: Int,
    totalTimeSpent: Int,
    humanTimeEstimate: Option[Int],
    humanTotalTimeSpend: Option[Int]
)
