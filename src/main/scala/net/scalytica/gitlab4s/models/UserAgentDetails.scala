package net.scalytica.gitlab4s.models

case class UserAgentDetails(
    userAgent: String,
    ipAddress: String,
    akismetSubmitted: Boolean
)
