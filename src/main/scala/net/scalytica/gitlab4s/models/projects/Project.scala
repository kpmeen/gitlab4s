package net.scalytica.gitlab4s.models.projects
import java.time.ZonedDateTime

case class ProjectOwner(
    id: Int,
    name: String,
    createdAt: ZonedDateTime
)

case class ProjectNamespace(
    id: Int,
    name: String,
    path: String,
    kind: String, // TODO ADT for namespace kind
    fullPath: String
)

case class ProjectGroup(
    groupId: Int,
    groupName: String,
    groupAccessLevel: Int
)

case class ProjectStats(
    commit_count: Int,
    storage_size: Int,
    repository_size: Int,
    lfs_objects_size: Int,
    job_artifacts_size: Int
)

class Project(
    id: Int,
    name: String,
    path: String,
    nameWithNamespace: String,
    pathWithNamespace: String,
    description: Option[String],
    avatarUrl: Option[String],
    defaultBranch: String,
    isPrivate: Boolean,
    owner: ProjectOwner,
    creatorId: Int,
    namespace: ProjectNamespace,
    sshUrlToRepo: String,
    httpUrlToRepo: String,
    webUrl: String,
    readmeUrl: Option[String],
    tagList: Seq[String],
    createdAt: ZonedDateTime,
    lastActivityAt: Option[ZonedDateTime],
    importStatus: Option[String], // TODO: encode alternatives as ADT?
    openIssuesCount: Int,
    forksCount: Int,
    starCount: Int,
    runnersToken: Option[String],
    archived: Boolean,
    sharedWithGroups: Seq[ProjectGroup],
    issuesEnabled: Boolean,
    mergeRequestEnabled: Boolean,
    jobsEnabled: Boolean,
    wikiEnabled: Boolean,
    snippetsEnabled: Boolean,
    resolveOutdatedDiffDiscussions: Boolean,
    containerRegistryEnabled: Boolean,
    sharedRunnersEnabled: Boolean,
    publicJobs: Boolean,
    onlyAllowMergeIfPipelineSucceeds: Boolean,
    onlyAllowMergeIfAllDiscussionsAreResolved: Boolean,
    requestAccessEnabled: Boolean,
    mergeMethod: String, // TODO: encode as ADT?
    approvalsBeforeMerge: Int,
    printingMergeRequestsLinkEnabled: Boolean,
    statistics: Option[ProjectStats],
    _links: Map[String, String]
)
