package net.scalytica.gitlab4s.models

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.users.SimpleUser

case class Comment(
    note: String,
    createdAt: ZonedDateTime,
    author: GitLabUser,
    line: Option[String],
    lineType: Option[LineType],
    path: Option[String]
)

sealed abstract class LineType(val value: String)
case object NewLine extends LineType("new")
case object OldLine extends LineType("old")
