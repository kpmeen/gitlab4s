package net.scalytica.gitlab4s.models

/*
  TODO: Supported durations for human*:
    - months (mo)
    - weeks (w)
    - days (d)
    - hours (h)
    - minutes (m)
 */

case class TimeEstimate(
    timeEstimate: Int, // In seconds
    totalTimeSpent: Int, // In Seconds
    humanTimeEstimate: Option[String], // FIXME: Data type???
    humanTotalTimeSpent: Option[String] // FIXME: Data type???
)
