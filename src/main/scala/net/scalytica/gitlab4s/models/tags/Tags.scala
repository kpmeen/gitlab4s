package net.scalytica.gitlab4s.models.tags

import net.scalytica.gitlab4s.models.commits.{Commit, CommitSha}

case class Release(tagName: String, description: Option[String])

object Release {
  val empty = Release("", None)
}

case class Tag(
    name: String,
    target: CommitSha,
    commit: Commit,
    message: Option[String] = None,
    release: Option[Release] = None
)

object Tag {
  val empty = Tag(
    name = "",
    target = CommitSha.empty,
    commit = Commit.empty
  )
}
