package net.scalytica.gitlab4s.models.milestones

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.ClosedOrActive

case class Milestone(
    id: Int,
    iid: Int,
    projectId: Int,
    title: String,
    state: ClosedOrActive,
    created_at: Option[ZonedDateTime],
    updated_at: Option[ZonedDateTime],
    due_date: Option[ZonedDateTime]
)
