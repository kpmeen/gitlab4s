package net.scalytica.gitlab4s.models.mergerequests

object AcceptResults {

  sealed trait AcceptResult

  case object Merged            extends AcceptResult
  case object Conflicts         extends AcceptResult
  case object NoLongerMergeable extends AcceptResult
  case object ShaNotAtHead      extends AcceptResult
  case object NotPermitted      extends AcceptResult

}
