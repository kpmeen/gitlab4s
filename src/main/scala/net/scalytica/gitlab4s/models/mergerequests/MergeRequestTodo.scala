package net.scalytica.gitlab4s.models.mergerequests
import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.{GitLabUser, Todo}
import net.scalytica.gitlab4s.models.projects.Project

case class MergeRequestTodo(
    id: Int,
    project: Project,
    author: GitLabUser,
    actionName: String,
    targetType: String,
    target: MergeRequest,
    targetUrl: String,
    body: Option[String],
    state: String,
    createdAt: ZonedDateTime
) extends Todo[MergeRequest]
