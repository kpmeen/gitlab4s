package net.scalytica.gitlab4s.models.mergerequests

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.commits.{CommitDiff, CommitSha}
import net.scalytica.gitlab4s.models.milestones.Milestone
import net.scalytica.gitlab4s.models.{GitLabUser, TimeStats}

case class MergeRequest(
    id: Int,
    iid: Int,
    projectId: Int,
    title: String,
    description: Option[String],
    targetBranch: String,
    targetProjectId: Int,
    sourceBranch: String,
    sourceProjectId: String,
    createdAt: ZonedDateTime,
    updatedAt: Option[ZonedDateTime],
    author: GitLabUser,
    assignee: Option[GitLabUser],
    milestone: Option[Milestone],
    labels: Seq[String],
    state: MergeState,
    workInProgress: Boolean,
    discussionLocked: Boolean,
    squash: Boolean,
    mergeWhenPipelineSucceeds: Boolean,
    shouldRemoveSourceBranch: Boolean,
    forceRemoveSourceBranch: Boolean,
    approvalsBeforeMerge: Option[Int],
    mergeStatus: Option[MergeStatus],
    sha: CommitSha,
    mergeCommitSha: Option[CommitSha],
    userNotesCount: Int,
    webUrl: String,
    timeStats: Option[TimeStats],
    downvotes: Int = 0,
    upvotes: Int = 0,
    changes: Option[Seq[CommitDiff]]
)
