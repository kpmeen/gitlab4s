package net.scalytica.gitlab4s.models.mergerequests

sealed abstract class MergeStatus(val value: String)

case object CanBeMerged              extends MergeStatus("can_be_merged")
case object CannotBeMerged           extends MergeStatus("cannot_be_merged")
case class UnknownStatus(in: String) extends MergeStatus(in)
