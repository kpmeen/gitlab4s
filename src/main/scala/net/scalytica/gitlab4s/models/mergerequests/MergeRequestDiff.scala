package net.scalytica.gitlab4s.models.mergerequests
import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.commits.{Commit, CommitDiff, CommitSha}

case class MergeRequestDiff(
    id: Int,
    mergeRequestId: Int,
    createdAt: ZonedDateTime,
    state: String, // TODO: To encode as ADT, need to know all possible states!
    realSize: Option[String], // *sigh* ... this is really an integer
    headCommitSha: Option[CommitSha],
    baseCommitSha: Option[CommitSha],
    startCommitSha: Option[CommitSha],
    commits: Option[Seq[Commit]],
    diffs: Option[Seq[CommitDiff]]
)
