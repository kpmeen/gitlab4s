package net.scalytica.gitlab4s.models.mergerequests

sealed abstract class MergeState(val value: String)

case object NoState extends MergeState("all")
case object Merged  extends MergeState("merged")
case object Opened  extends MergeState("opened")
case object Closed  extends MergeState("closed")
case object Locked  extends MergeState("locked")
