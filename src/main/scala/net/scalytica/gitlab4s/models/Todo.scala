package net.scalytica.gitlab4s.models
import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.projects.Project

trait Todo[Target] {
  val id: Int
  val project: Project
  val author: GitLabUser
  val actionName: String // TODO: create ADT?
  val targetType: String // TODO: create ADT?
  val target: Target
  val targetUrl: String
  val body: Option[String]
  val state: String // TODO: create ADT?
  val createdAt: ZonedDateTime
}
