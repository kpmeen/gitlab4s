package net.scalytica.gitlab4s.models

import io.circe.{Decoder, Encoder, Json}

sealed trait ClosedOrActive
case object Closed extends ClosedOrActive
case object Active extends ClosedOrActive

object ClosedOrActive {

  implicit val encodeState: Encoder[ClosedOrActive] =
    Encoder.instance {
      case Closed => Json.fromString("closed")
      case Active => Json.fromString("active")
    }
  implicit val decodeState: Decoder[ClosedOrActive] =
    Decoder.decodeString.emap {
      case str if str == "closed" => Right(Closed)
      case str if str == "active" => Right(Active)
      case str                    => Left(s"$str is not a known state")
    }

}
