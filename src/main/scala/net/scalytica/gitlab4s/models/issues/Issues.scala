package net.scalytica.gitlab4s.models.issues

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.milestones.Milestone
import net.scalytica.gitlab4s.models.{ClosedOrActive, GitLabUser, TimeEstimate}

case class Issue(
    id: Int,
    iid: Int,
    projectId: Int,
    title: String,
    state: ClosedOrActive,
    webUrl: String,
    createdAt: ZonedDateTime,
    userNotesCount: Option[Int],
    confidential: Option[Boolean],
    discussionLocked: Option[Boolean],
    description: Option[String],
    labels: Option[Seq[String]],
    weight: Option[Int],
    updatedAt: Option[ZonedDateTime],
    dueDate: Option[ZonedDateTime],
    closedAt: Option[ZonedDateTime],
    closedBy: Option[GitLabUser],
    milestone: Option[Milestone],
    author: Option[GitLabUser],
    assignees: Option[Seq[GitLabUser]],
    assignee: Option[GitLabUser],
    timeStats: Option[TimeEstimate]
)
