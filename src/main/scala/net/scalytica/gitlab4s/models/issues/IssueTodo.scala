package net.scalytica.gitlab4s.models.issues
import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.{GitLabUser, Todo}
import net.scalytica.gitlab4s.models.projects.Project

case class IssueTodo(
    id: Int,
    project: Project,
    author: GitLabUser,
    actionName: String, // TODO: create ADT?
    targetType: String, // TODO: create ADT?
    target: Issue,
    targetUrl: String,
    body: Option[String],
    state: String, // TODO: create ADT?
    createdAt: ZonedDateTime
) extends Todo[Issue]
