package net.scalytica.gitlab4s.models

sealed abstract class Scope(val value: String)

case object CreatedByMe  extends Scope("created_by_me")
case object AssignedToMe extends Scope("assigned_to_me")
case object All          extends Scope("all")
