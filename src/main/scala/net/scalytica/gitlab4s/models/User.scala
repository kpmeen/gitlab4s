package net.scalytica.gitlab4s.models

import io.circe.{Decoder, Encoder, Json}

sealed trait UserState
case object ActiveUser   extends UserState
case object InactiveUser extends UserState
case object BlockedUser  extends UserState

object UserState {
  implicit val encodeUserState: Encoder[UserState] =
    Encoder.instance {
      case ActiveUser   => Json.fromString("active")
      case InactiveUser => Json.fromString("inactive")
      case BlockedUser  => Json.fromString("blocked")
    }

  implicit val decodeUserState: Decoder[UserState] =
    Decoder.decodeString.emap {
      case str if str == "active"   => Right(ActiveUser)
      case str if str == "inactive" => Right(InactiveUser)
      case str if str == "blocked"  => Right(BlockedUser)
      case str                      => Left(s"$str is not a known state")
    }
}

case class GitLabUser(
    id: Int,
    username: String,
    state: UserState,
    webUrl: String,
    name: Option[String],
    avatarUrl: Option[String]
)
