package net.scalytica.gitlab4s.models.commits

import java.time.ZonedDateTime

import net.scalytica.gitlab4s._
import net.scalytica.gitlab4s.models.pipelines.PipelineStatus

case class Commit(
    id: CommitSha,
    shortId: CommitShortSha,
    title: String,
    createdAt: ZonedDateTime,
    message: Option[String] = None,
    committerName: Option[String] = None,
    committerEmail: Option[String] = None,
    committedDate: Option[ZonedDateTime] = None,
    authorName: Option[String] = None,
    authorEmail: Option[String] = None,
    authoredDate: Option[ZonedDateTime] = None,
    parentIds: Option[List[CommitSha]] = None,
    lastPipeline: Option[PipelineStatus] = None,
    stats: Option[CommitStats] = None
)

object Commit {
  val empty = Commit(
    id = CommitSha.empty,
    shortId = CommitShortSha.empty,
    title = "",
    createdAt = EmptyZonedDateTime
  )
}
