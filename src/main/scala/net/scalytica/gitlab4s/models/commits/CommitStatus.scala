package net.scalytica.gitlab4s.models.commits

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.pipelines.PipelineStatus
import net.scalytica.gitlab4s.models.users.SimpleUser

case class CommitStatus(
    id: Int,
    allowFailure: Boolean,
    sha: CommitSha,
    ref: String,
    targetUrl: String,
    author: SimpleUser,
    status: PipelineStatus,
    createdAt: ZonedDateTime,
    name: Option[String],
    description: Option[String],
    startedAt: Option[ZonedDateTime],
    finishedAt: Option[ZonedDateTime]
)
