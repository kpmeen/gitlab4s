package net.scalytica.gitlab4s.models.commits

case class AddCommit(
    branch: String,
    commitMessage: String,
    startBranch: Option[String],
    actions: Seq[CommitAction],
    authorEmail: Option[String],
    authorName: Option[String]
)
