package net.scalytica.gitlab4s.models.commits

case class CommitDiff(
    diff: String,
    newPath: Option[String],
    oldPath: Option[String],
    aMode: Option[String],
    bMode: Option[String],
    newFile: Boolean,
    renamedFile: Boolean,
    deletedFile: Boolean
)
