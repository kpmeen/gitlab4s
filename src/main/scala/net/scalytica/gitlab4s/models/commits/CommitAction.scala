package net.scalytica.gitlab4s.models.commits

sealed trait CommitAction {
  val action: String
  val filePath: String
  val encoding: CommitEncoding
  val previousPath: Option[String]
}

/**
 *
 * @param action       The action to perform, create, delete, move, update
 * @param filePath     Full path to the file. Ex. lib/class.rb
 * @param content      File content.
 * @param encoding     text or base64. text is default.
 * @param previousPath Original full path to the file being moved.
 */
case class CreateCommit(
    action: String,
    filePath: String,
    content: String,
    encoding: CommitEncoding = Text,
    previousPath: Option[String] = None
) extends CommitAction

/**
 *
 * @param action       The action to perform, create, delete, move, update
 * @param filePath     Full path to the file. Ex. lib/class.rb
 * @param content      File content.
 * @param encoding     text or base64. text is default.
 * @param previousPath Original full path to the file being moved.
 * @param lastCommitId Last known file commit id.
 */
case class UpdateCommit(
    action: String,
    filePath: String,
    content: String,
    encoding: CommitEncoding = Text,
    previousPath: Option[String] = None,
    lastCommitId: Option[String] = None
) extends CommitAction

/**
 *
 * @param action       The action to perform, create, delete, move, update
 * @param filePath     Full path to the file. Ex. lib/class.rb
 * @param encoding     text or base64. text is default.
 * @param previousPath Original full path to the file being moved.
 * @param lastCommitId Last known file commit id.
 */
case class DeleteCommit(
    action: String,
    filePath: String,
    encoding: CommitEncoding = Text,
    previousPath: Option[String] = None,
    lastCommitId: Option[String] = None
) extends CommitAction

/**
 *
 * @param action       The action to perform, create, delete, move, update
 * @param filePath     Full path to the file. Ex. lib/class.rb
 * @param content      File content.
 * @param encoding     text or base64. text is default.
 * @param previousPath Original full path to the file being moved.
 * @param lastCommitId Last known file commit id.
 */
case class MoveCommit(
    action: String,
    filePath: String,
    content: Option[String] = None,
    encoding: CommitEncoding = Text,
    previousPath: Option[String] = None,
    lastCommitId: Option[String] = None
) extends CommitAction
