package net.scalytica.gitlab4s.models.commits

import io.circe.{Decoder, Encoder, Json}

case class CommitSha(value: String) {
  require(value.length == 40)
}

object CommitSha {
  val empty = CommitSha("0000000000000000000000000000000000000000")

  implicit val encodeCommitSha: Encoder[CommitSha] =
    Encoder.instance(v => Json.fromString(v.value))

  implicit val decodeCommitSha: Decoder[CommitSha] =
    Decoder.decodeString.emap(s => Right(CommitSha(s)))
}
