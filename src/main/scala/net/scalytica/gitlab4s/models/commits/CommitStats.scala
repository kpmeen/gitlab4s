package net.scalytica.gitlab4s.models.commits

case class CommitStats(
    additions: Option[Int] = None,
    deletions: Option[Int] = None,
    total: Option[Int] = None
)
