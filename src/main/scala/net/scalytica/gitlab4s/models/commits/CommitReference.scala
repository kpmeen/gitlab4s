package net.scalytica.gitlab4s.models.commits

case class CommitReference(`type`: String, name: String)

case object CommitReference {

  sealed abstract class ReferenceType(val value: String)
  case object Branch extends ReferenceType("branch")
  case object Tag    extends ReferenceType("tag")
  case object All    extends ReferenceType("all")

}
