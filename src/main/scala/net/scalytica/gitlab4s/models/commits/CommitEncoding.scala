package net.scalytica.gitlab4s.models.commits

sealed abstract class CommitEncoding(val value: String)

case object Text   extends CommitEncoding("text")
case object Base64 extends CommitEncoding("base64")
