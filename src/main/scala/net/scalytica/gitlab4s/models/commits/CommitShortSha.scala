package net.scalytica.gitlab4s.models.commits

import io.circe.{Decoder, Encoder, Json}

case class CommitShortSha(value: String) extends AnyVal

object CommitShortSha {
  val empty = CommitShortSha("00000000000")

  implicit val encodeCommitShortSha: Encoder[CommitShortSha] =
    Encoder.instance(v => Json.fromString(v.value))

  implicit val decodeCommitShortSha: Decoder[CommitShortSha] =
    Decoder.decodeString.emap(s => Right(CommitShortSha(s)))
}
