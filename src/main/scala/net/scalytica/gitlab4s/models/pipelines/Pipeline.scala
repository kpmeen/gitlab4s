package net.scalytica.gitlab4s.models.pipelines

import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.GitLabUser
import net.scalytica.gitlab4s.models.commits.CommitSha

/*

{

  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-08-11T11:28:34.085Z",
  "updated_at": "2016-08-11T11:32:35.169Z",
  "started_at": null,
  "finished_at": "2016-08-11T11:32:35.145Z",
  "committed_at": null,
  "duration": null,
  "coverage": "30.0"
}


 */

case class Pipeline(
    id: Int,
    ref: String,
    status: PipelineStatus,
    sha: CommitSha,
    beforeSha: Option[CommitSha],
    tag: Option[Boolean],
    yamlErrors: Option[String],
    user: Option[GitLabUser],
    createdAt: ZonedDateTime,
    updatedAt: Option[ZonedDateTime],
    startedAt: Option[ZonedDateTime],
    finishedAt: Option[ZonedDateTime],
    committedAt: Option[ZonedDateTime],
    duration: Option[Long],
    coverage: Option[Float]
)
