package net.scalytica.gitlab4s.models.pipelines

import io.circe.{Decoder, Encoder, Json}

sealed abstract class PipelineStatus(val value: String)
case object Running  extends PipelineStatus("running")
case object Pending  extends PipelineStatus("pending")
case object Success  extends PipelineStatus("success")
case object Failed   extends PipelineStatus("failed")
case object Canceled extends PipelineStatus("canceled")
case object Skipped  extends PipelineStatus("skipped")

object PipelineStatus {

  implicit val encodePipelineStatus: Encoder[PipelineStatus] =
    Encoder.instance {
      case Running  => Json.fromString(Running.value)
      case Pending  => Json.fromString(Pending.value)
      case Success  => Json.fromString(Success.value)
      case Failed   => Json.fromString(Failed.value)
      case Canceled => Json.fromString(Canceled.value)
      case Skipped  => Json.fromString(Skipped.value)
    }

  implicit val decodePipelineStatus: Decoder[PipelineStatus] =
    Decoder.decodeString.emap {
      case str if str == Running.value  => Right(Running)
      case str if str == Pending.value  => Right(Pending)
      case str if str == Success.value  => Right(Success)
      case str if str == Failed.value   => Right(Failed)
      case str if str == Canceled.value => Right(Canceled)
      case str if str == Skipped.value  => Right(Skipped)
      case str                          => Left(s"$str is not a known state")
    }

}
