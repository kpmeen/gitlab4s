package net.scalytica

import java.time.{LocalDate, ZoneId, ZonedDateTime}

import scala.math.Ordering

package object gitlab4s {

  object OrderingImplicits {
    implicit val sortedZonedDateTimeAscSeq: Ordering[ZonedDateTime] =
      Ordering.by { zdt: ZonedDateTime =>
        zdt.toInstant.toEpochMilli
      }

    implicit val sortedLocalDateAscSeq: Ordering[LocalDate] =
      Ordering.by { ld: LocalDate =>
        ld.atStartOfDay(ZoneId.of("UTC")).toEpochSecond
      }
  }

  implicit class ISO_8601_DateTime(zd: ZonedDateTime) {

    def toIso8601: String = zd.toOffsetDateTime.toString

  }

  /**
   * ZonedDateTime used to indicate an empty date. There's not much chance of a
   * commit having been done in year 0. (ಠ‿↼)
   */
  val EmptyZonedDateTime: ZonedDateTime =
    ZonedDateTime.of(0, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"))

}
