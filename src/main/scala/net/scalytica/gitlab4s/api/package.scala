package net.scalytica.gitlab4s

import cats.effect.IO

package object api {

  type GitLabIO[T] = IO[T]

}
