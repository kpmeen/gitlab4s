package net.scalytica.gitlab4s.api

import java.time.ZonedDateTime

import cats.syntax.either._
import io.circe._
import io.circe.generic.extras.Configuration
import net.scalytica.gitlab4s.client._

trait ApiBase extends ExtraJsonCodecs {

  val MaxResultsPerPage = 100

  final protected def baseUrl(implicit ctx: GitLabContext): GitLabUrl =
    GitLabUrl(value = s"${ctx.gitlabHost.url}/api/${ctx.apiVersion}")

  final protected def BaseProjUrl(implicit ctx: GitLabContext): GitLabUrl =
    baseUrl.append(s"/projects/${ctx.projectUri.urlEncoded}")

}

trait ExtraJsonCodecs {

  implicit val snakeCaseConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames

  implicit val encodeInstant: Encoder[ZonedDateTime] =
    Encoder.encodeString.contramap[ZonedDateTime](_.toString)

  implicit val decodeInstant: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { str =>
      Either
        .catchNonFatal(ZonedDateTime.parse(str))
        .leftMap(t => s"ZonedDateTime could not be parsed: ${t.getMessage}")
    }

}
