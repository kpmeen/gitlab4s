package net.scalytica.gitlab4s.api

import net.scalytica.gitlab4s.client.{GitLabContext, GitLabUrl}

trait MilestonesApi extends ApiBase {

  def MilestonesUrl(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("milestones")

}
