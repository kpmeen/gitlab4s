package net.scalytica.gitlab4s.api

import java.time.{LocalDate, ZonedDateTime}

import io.circe._
import io.circe.generic.extras.auto._
import net.scalytica.gitlab4s._
import net.scalytica.gitlab4s.api.QueryParams._
import net.scalytica.gitlab4s.client.{GitLabClient, GitLabContext, GitLabUrl}
import net.scalytica.gitlab4s.models.issues.{Issue, IssueTodo}
import net.scalytica.gitlab4s.models.mergerequests.MergeRequest
import net.scalytica.gitlab4s.models.{
  GitLabUser,
  TimeEstimate,
  UserAgentDetails,
  Scope => IssueScope
}

trait IssuesApi extends ApiBase {

  def IssuesUrl(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("issues")

  def GroupIssuesUrl(groupId: String)(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("groups").append(groupId).append("issues")

  def ProjectIssuesUrl(
      projectId: String
  )(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("projects").append(projectId).append("issues")

  implicit val issueSeqDecoder: Decoder[Seq[Issue]] = Decoder.decodeSeq[Issue]
  implicit val issueOptDecoder: Decoder[Option[Issue]] =
    Decoder.decodeOption[Issue]

  private[this] def issues(url: GitLabUrl)(
      state: Option[String],
      labels: Seq[String],
      milestone: Option[String],
      scope: Option[IssueScope],
      authorId: Option[Int],
      assigneeId: Option[Int],
      myReactionEmoji: Option[String],
      iids: Seq[Int],
      orderBy: Option[String],
      sort: Option[String],
      search: Option[String],
      createdAfter: Option[ZonedDateTime],
      createdBefore: Option[ZonedDateTime],
      updatedAfter: Option[ZonedDateTime],
      updatedBefore: Option[ZonedDateTime],
      page: Int,
      perPage: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Issue]] = {
    val params = Seq.newBuilder[(String, String)]
    state.foreach(s => params += State                     -> s)
    milestone.foreach(m => params += Milestone             -> m)
    scope.foreach(s => params += Scope                     -> s.value)
    authorId.foreach(a => params += AuthorId               -> a.toString)
    myReactionEmoji.foreach(m => params += MyReactionEmoji -> m)
    orderBy.foreach(o => params += OrderBy                 -> o)
    sort.foreach(s => params += Sort                       -> s)
    search.foreach(s => params += Search                   -> s)
    createdAfter.foreach(c => params += CreatedAfter       -> c.toIso8601)
    createdBefore.foreach(c => params += CreatedBefore     -> c.toIso8601)
    updatedAfter.foreach(u => params += UpdatedAfter       -> u.toIso8601)
    updatedBefore.foreach(u => params += UpdatedBefore     -> u.toIso8601)
    if (labels.nonEmpty) params += Labels -> labels.mkString(",")
    if (iids.nonEmpty) params += IidArray -> iids.mkString(",")

    client.paged[Issue](
      gitlabUrl = url.withQueryParams(params.result(): _*),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get all issues the authenticated user has access to. By default it returns
   * only issues created by the current user. To get all issues, use parameter
   * scope=all
   *
   * @param state Return all issues or just those that are opened or closed.
   * @param labels Comma-separated list of label names, issues must have all
   *               labels to be returned. No+Label lists all issues with no
   *               labels.
   * @param milestone The milestone title. No+Milestone lists all issues with no
   *                  milestone.
   * @param scope Return issues for the given scope: created_by_me,
   *              assigned_to_me or all. Defaults to created_by_me.
   *              assigned-to-me scopes instead.
   * @param authorId Return issues created by the given user id. Combine with
   *                 scope=all or scope=assigned_to_me.
   * @param assigneeId Return issues assigned to the given user id.
   * @param myReactionEmoji Return issues reacted by the authenticated user by
   *                        the given emoji.
   * @param iids Return only the issues having the given iid.
   * @param orderBy Return issues ordered by created_at or updated_at fields.
   *                Default is created_at.
   * @param sort Return issues sorted in asc or desc order. Default is desc.
   * @param search Search issues against their title and description.
   * @param createdAfter Return issues created on or after the given time.
   * @param createdBefore Return issues created on or before the given time.
   * @param updatedAfter Return issues updated on or after the given time.
   * @param updatedBefore Return issues updated on or before the given time.
   * @param page The page number to retrieve.
   * @param perPage Number of issues per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Issue]]s.
   */
  def issuesForUser(
      state: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      milestone: Option[String] = None,
      scope: Option[IssueScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      iids: Seq[Int] = Seq.empty,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      search: Option[String] = None,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Issue]] = issues(IssuesUrl)(
    state = state,
    labels = labels,
    milestone = milestone,
    scope = scope,
    authorId = authorId,
    assigneeId = assigneeId,
    myReactionEmoji = myReactionEmoji,
    iids = iids,
    orderBy = orderBy,
    sort = sort,
    search = search,
    createdAfter = createdAfter,
    createdBefore = createdBefore,
    updatedAfter = updatedAfter,
    updatedBefore = updatedBefore,
    page = page,
    perPage = perPage
  )

  /**
   * Get a list of a group's issues.
   *
   * @param groupId The ID or path of the group owned by the authenticated user
   * @param state Return all issues or just those that are opened or closed
   * @param labels Comma-separated list of label names, issues must have all
   *               labels to be returned. No+Label lists all issues with no
   *               labels.
   * @param milestone The milestone title. No+Milestone lists all issues with no
   *                  milestone.
   * @param scope Return issues for the given scope: created_by_me,
   *              assigned_to_me or all.
   * @param authorId Return issues created by the given user id.
   * @param assigneeId Return issues assigned to the given user id.
   * @param myReactionEmoji Return issues reacted by the authenticated user by
   *                        the given emoji.
   * @param iids Return only the issues having the given iid.
   * @param orderBy Return issues ordered by created_at or updated_at fields.
   *                Default is created_at
   * @param sort Return issues sorted in asc or desc order. Default is desc.
   * @param search Search group issues against their title and description.
   * @param createdAfter Return issues created on or after the given time.
   * @param createdBefore Return issues created on or before the given time.
   * @param updatedAfter Return issues updated on or after the given time.
   * @param updatedBefore Return issues updated on or before the given time.
   * @param page The page number to retrieve.
   * @param perPage Number of issues per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Issue]]s.
   */
  def issuesForGroup(
      groupId: String,
      state: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      milestone: Option[String] = None,
      scope: Option[IssueScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      iids: Seq[Int] = Seq.empty,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      search: Option[String] = None,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Issue]] = issues(GroupIssuesUrl(groupId))(
    state = state,
    labels = labels,
    milestone = milestone,
    scope = scope,
    authorId = authorId,
    assigneeId = assigneeId,
    myReactionEmoji = myReactionEmoji,
    iids = iids,
    orderBy = orderBy,
    sort = sort,
    search = search,
    createdAfter = createdAfter,
    createdBefore = createdBefore,
    updatedAfter = updatedAfter,
    updatedBefore = updatedBefore,
    page = page,
    perPage = perPage
  )

  /**
   * Get a list of a project's issues.
   *
   * @param projectId The ID or path of the project owned by the authenticated
   *                  user.
   * @param state Return all issues or just those that are opened or closed.
   * @param labels Comma-separated list of label names, issues must have all
   *               labels to be returned. No+Label lists all issues with no
   *               labels.
   * @param milestone The milestone title. No+Milestone lists all issues with no
   *                  milestone.
   * @param scope Return issues for the given scope: created_by_me,
   *              assigned_to_me or all.
   * @param authorId Return issues created by the given user id.
   * @param assigneeId Return issues assigned to the given user id.
   * @param myReactionEmoji Return issues reacted by the authenticated user by
   *                        the given emoji.
   * @param iids Return only the issues having the given iid.
   * @param orderBy Return issues ordered by created_at or updated_at fields.
   *                Default is created_at
   * @param sort Return issues sorted in asc or desc order. Default is desc.
   * @param search Search group issues against their title and description.
   * @param createdAfter Return issues created on or after the given time.
   * @param createdBefore Return issues created on or before the given time.
   * @param updatedAfter Return issues updated on or after the given time.
   * @param updatedBefore Return issues updated on or before the given time.
   * @param page The page number to retrieve.
   * @param perPage Number of issues per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Issue]]s.
   */
  def issuesForProject(
      projectId: String,
      state: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      milestone: Option[String] = None,
      scope: Option[IssueScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      iids: Seq[Int] = Seq.empty,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      search: Option[String] = None,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Issue]] = issues(ProjectIssuesUrl(projectId))(
    state = state,
    labels = labels,
    milestone = milestone,
    scope = scope,
    authorId = authorId,
    assigneeId = assigneeId,
    myReactionEmoji = myReactionEmoji,
    iids = iids,
    orderBy = orderBy,
    sort = sort,
    search = search,
    createdAfter = createdAfter,
    createdBefore = createdBefore,
    updatedAfter = updatedAfter,
    updatedBefore = updatedBefore,
    page = page,
    perPage = perPage
  )

  /**
   * Get a single project issue.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Issue]]s.
   */
  def issue(projectId: String, issueIid: String)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[Issue]] =
    client.get(ProjectIssuesUrl(projectId).append(issueIid))

  /**
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user
   * @param title The title of the issue
   * @param iid The internal ID of the project's issue. Requires admin or
   *            project owner rights.
   * @param description The description of the issue.
   * @param confidential Set an issue to be confidential. Default is false.
   * @param assigneeIds The user ID's to assign the issue.
   * @param milestoneId The global ID of a milestone to assign issue.
   * @param labels Comma-separated label names for the issue.
   * @param createdAt Date and time for the when the issue was created. Requires
   *                  admin or project owner rights.
   * @param dueDate The local date (YYYY-MM-DD) the issue is due.
   * @param mergeRequestToResolveDiscussionsOf
   *             The IID of a merge request in which to resolve all issues. This
   *             will fill the issue with a default description and mark all
   *             discussions as resolved. When passing a description or title,
   *             these values will take precedence over the default values.
   * @param discussionToResolve The ID of a discussion to resolve. This will
   *                            fill in the issue with a default description and
   *                            mark the discussion as resolved. Use in
   *                            combination with
   *                            mergeRequestToResolveDiscussionsOf.
   * @param weight The weight of the issue in range 0 to 9
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the added [[Issue]]
   */
  def create(
      projectId: String,
      title: String,
      iid: Option[String] = None,
      description: Option[String] = None,
      confidential: Boolean = false,
      assigneeIds: Seq[Int] = Seq.empty,
      milestoneId: Option[Int] = None,
      labels: Seq[String] = Seq.empty,
      createdAt: Option[ZonedDateTime] = None,
      dueDate: Option[LocalDate] = None,
      mergeRequestToResolveDiscussionsOf: Option[Int] = None,
      discussionToResolve: Option[String] = None,
      weight: Option[Int] = None
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Issue] = {
    val params = Seq.newBuilder[(String, String)]
    params += Title -> title
    iid.foreach(i => params += Iid                 -> i)
    description.foreach(d => params += Description -> d)
    params += Confidential -> confidential.toString
    milestoneId.foreach(m => params += MilestoneId -> m.toString)
    createdAt.foreach(c => params += CreatedAt     -> c.toIso8601)
    dueDate.foreach(d => params += DueDate         -> d.toString)
    weight.foreach(w => params += Weight           -> w.toString)
    mergeRequestToResolveDiscussionsOf.foreach { m =>
      params += MergeRequestToResolveDiscussionsOf -> m.toString
    }
    discussionToResolve.foreach(d => params += DiscussionToResolve -> d)
    if (assigneeIds.nonEmpty) params += AssigneeIds -> assigneeIds.mkString(",")
    if (labels.nonEmpty) params += Labels           -> labels.mkString(",")

    client.postNoBody(
      ProjectIssuesUrl(projectId).withQueryParams(params.result(): _*)
    )
  }

  /**
   * Updates an existing project issue. This call is also used to mark an issue
   * as closed.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *           authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param title The title of an issue.
   * @param description The description of an issue.
   * @param confidential Updates an issue to be confidential.
   * @param assigneeIds The ID of the user(s) to assign the issue to. Set to 0
   *                    or provide an empty value to unassign all assignees.
   * @param milestoneId The global ID of a milestone to assign the issue to. Set
   *                    to 0 or provide an empty value to unassign a milestone.
   * @param labels Comma-separated label names for an issue. Set to an empty
   *               string to unassign all labels.
   * @param stateEvent The state event of an issue. Set close to close the issue
   *                   and reopen to reopen it.
   * @param updatedAt Date time string, ISO 8601 formatted, e.g.
   *                  2016-03-11T03:45:40Z.
   *                  Requires admin or project owner rights.
   * @param dueDate Date time string in the format YEAR-MONTH-DAY.
   * @param weight The weight of the issue in range 0 to 9
   * @param discussionLocked Flag indicating if the issue's discussion is
   *                         locked. If the discussion is locked only project
   *                         members can add or edit comments.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the added [[Issue]]
   */
  def update(
      projectId: String,
      issueIid: Int,
      title: Option[String] = None,
      description: Option[String] = None,
      confidential: Option[Boolean] = None,
      assigneeIds: Option[Seq[Int]] = None,
      milestoneId: Option[Int] = None,
      labels: Option[String] = None,
      stateEvent: Option[String] = None,
      updatedAt: Option[ZonedDateTime] = None,
      dueDate: Option[LocalDate] = None,
      weight: Option[Int] = None,
      discussionLocked: Option[Boolean] = None
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Issue] = {
    val params = Seq.newBuilder[(String, String)]
    title.foreach(t => params += Title                       -> t)
    description.foreach(d => params += Description           -> d)
    confidential.foreach(c => params += Confidential         -> c.toString)
    assigneeIds.foreach(a => params += AssigneeIds           -> a.toString)
    milestoneId.foreach(m => params += MilestoneId           -> m.toString)
    labels.foreach(l => params += Labels                     -> l)
    stateEvent.foreach(s => params += StateEvent             -> s)
    updatedAt.foreach(u => params += UpdatedAt               -> u.toIso8601)
    dueDate.foreach(d => params += DueDate                   -> d.toString)
    weight.foreach(w => params += Weight                     -> w.toString)
    discussionLocked.foreach(d => params += DiscussionLocked -> d.toString)

    client.putNoBody(
      ProjectIssuesUrl(projectId)
        .append(issueIid)
        .withQueryParams(params.result(): _*)
    )
  }

  /**
   * Only for admins and project owners. Soft deletes the issue in question.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad of Unit
   */
  def delete(projectId: String, issueIid: Int)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Unit] =
    client.delete(ProjectIssuesUrl(projectId).append(issueIid))

  /**
   * Moves an issue to a different project. If the target project equals the
   * source project or the user has insufficient permissions to move an issue,
   * error 400 together with an explaining error message is returned.
   *
   * If a given label and/or milestone with the same name also exists in the
   * target project, it will then be assigned to the issue that is being moved.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param toProjectId The ID of the project to move the issue to.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the moved [[Issue]]
   */
  def move(
      projectId: String,
      issueIid: Int,
      toProjectId: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Issue] =
    client.post(
      gitlabUrl = ProjectIssuesUrl(projectId).append(issueIid),
      data = s"""{"$ToProjectId": $toProjectId}"""
    )

  /**
   * Subscribes the authenticated user to an issue to receive notifications.
   * If the user is already subscribed to the issue, the status code 304
   * is returned.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing the [[Issue]] being subscribed to.
   */
  def subscribe(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Issue] =
    client.postNoBody(
      ProjectIssuesUrl(projectId).append(issueIid).append("subscribe")
    )

  /**
   * Un-subscribes the authenticated user from the issue to not receive
   * notifications from it. If the user is not subscribed to the issue, the
   * status code 304 is returned.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing the unsubscribed [[Issue]].
   */
  def unsubscribe(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Issue] = client.postNoBody(
    ProjectIssuesUrl(projectId).append(issueIid).append("unsubscribe")
  )

  /**
   * Manually creates a todo for the current user on an issue. If there already
   * exists a todo for the user on that issue, status code 304 is returned.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing an [[IssueTodo]].
   */
  def createTodo(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[IssueTodo] = client.postNoBody(
    ProjectIssuesUrl(projectId).append(issueIid).append("todo")
  )

  /**
   * Sets an estimated time of work for this issue.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param duration The duration in human format. e.g: 3h30m
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing a [[TimeEstimate]].
   */
  def addTimeEstimate(
      projectId: String,
      issueIid: Int,
      duration: String
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = client.postNoBody(
    ProjectIssuesUrl(projectId)
      .append(issueIid)
      .append("time_estimate")
      .withQueryParams("duration" -> duration)
  )

  /**
   * Resets the estimated time for this issue to 0 seconds.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing an [[TimeEstimate]].
   */
  def resetTimeEstimate(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = client.postNoBody(
    ProjectIssuesUrl(projectId).append(issueIid).append("reset_time_estimate")
  )

  /**
   * Adds spent time for this issue.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param duration The duration in human format. e.g: 3h30m
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing a [[TimeEstimate]].
   */
  def addTimeSpent(
      projectId: String,
      issueIid: Int,
      duration: String
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = client.postNoBody(
    ProjectIssuesUrl(projectId)
      .append(issueIid)
      .append("add_spent_time")
      .withQueryParams(TimeDuration -> duration)
  )

  /**
   * Resets the total spent time for this issue to 0 seconds.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing an [[TimeEstimate]].
   */
  def resetTimeSpent(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = client.postNoBody(
    ProjectIssuesUrl(projectId).append(issueIid).append("reset_spent_time")
  )

  /**
   * Get time tracking stats.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing an Option of [[TimeEstimate]].
   */
  def timeStats(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[TimeEstimate]] = client.get(
    ProjectIssuesUrl(projectId).append(issueIid).append("time_stats")
  )

  /**
   * List all merge requests that will close the given issue on merge.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing a collection of [[MergeRequest]].
   */
  def closedBy(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] =
    client.all(ProjectIssuesUrl(projectId).append(issueIid).append("closed_by"))

  /**
   * List all participants for an issue.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing a collection of [[GitLabUser]].
   */
  def participants(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[GitLabUser]] =
    client.all(
      ProjectIssuesUrl(projectId).append(issueIid).append("participants")
    )

  /**
   * Get user agent details. Available only for admins.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param issueIid The internal ID of a project's issue.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad containing an Option of [[UserAgentDetails]].
   */
  def userAgentDetails(
      projectId: String,
      issueIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[UserAgentDetails]] = client.get(
    ProjectIssuesUrl(projectId).append(issueIid).append("user_agent_detail")
  )
}
