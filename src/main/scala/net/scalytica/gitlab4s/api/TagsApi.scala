package net.scalytica.gitlab4s.api

import io.circe._
import io.circe.generic.extras.auto._
import net.scalytica.gitlab4s.api.QueryParams._
import net.scalytica.gitlab4s.client.{GitLabClient, GitLabContext, GitLabUrl}
import net.scalytica.gitlab4s.models.tags.{Release, Tag}

trait TagsApi extends ApiBase {

  def TagsUrl(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("/repository/tags")

  implicit val tagSeqDecoder: Decoder[Seq[Tag]]    = Decoder.decodeSeq[Tag]
  implicit val tagOptDecoder: Decoder[Option[Tag]] = Decoder.decodeOption[Tag]

  /**
   * Get a list of tags in a project.
   *
   * @param orderBy Optional argument defining which field to order by
   * @param sortDescending Boolean indicating sorting desc if true else asc
   * @param page      The page number to retrieve
   * @param perPage   Number of commits per page
   * @param ctx       The context to use for this API call
   * @param client    The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[Tag]]s
   */
  def tags(
      orderBy: Option[String] = None,
      sortDescending: Boolean = true,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(implicit ctx: GitLabContext, client: GitLabClient): GitLabIO[Seq[Tag]] =
    client.paged(
      gitlabUrl = TagsUrl
        .withPerPage(perPage)
        .withQueryParams(
          OrderBy -> "updated",
          Sort    -> (if (sortDescending) "desc" else "asc")
        ),
      page = page,
      perPage = perPage
    )

  /**
   * Get a single tag from a repository.
   *
   * @param tagName The name of the tag to get
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with an Optional of [[Tag]]
   */
  def tag(tagName: String)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[Tag]] =
    client.get(TagsUrl.append(tagName))

  /**
   * Creates a new tag in the repository that points to the supplied ref.
   *
   * @param tagName The name of the tag to add
   * @param ref The commit SHA, tag name or branch name to create a tag from
   * @param message The tag message
   * @param releaseDescription Release notes text for the tag.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad with the newly created [[Tag]]
   */
  def create(
      tagName: String,
      ref: String,
      message: Option[String],
      releaseDescription: Option[String]
  )(implicit ctx: GitLabContext, client: GitLabClient): GitLabIO[Tag] = {
    val params = Seq.newBuilder[(String, String)]
    params += Ref -> ref
    message.foreach(m => Message                        -> m)
    releaseDescription.foreach(rd => ReleaseDescription -> rd)

    client.postNoBody(gitlabUrl = TagsUrl.withQueryParams(params.result(): _*))
  }

  /**
   * Deletes a tag of a repository with given name.
   *
   * @param tagName The name of the tag to delete
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad of Unit
   */
  def delete(
      tagName: String
  )(implicit ctx: GitLabContext, client: GitLabClient): GitLabIO[Unit] =
    client.delete(TagsUrl.withQueryParams(TagName -> tagName))

  /**
   * Add release notes to the existing git tag.
   * (If a release exists for the given tag, status code 409 is returned.)
   *
   * @param tagName The name of the tag to create a release for
   * @param description The release notes description, with markdown support.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the added [[Release]]
   */
  def addRelease(tagName: String, description: String)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Release] = {
    client.postNoBody(
      TagsUrl
        .append(tagName)
        .append("release")
        .withQueryParams(Description -> description)
    )
  }

  /**
   * Updates the release notes of a given release.
   *
   * @param tagName The name of the tag to update the release for
   * @param description The release notes description, with markdown support.
   * @param ctx The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the updated [[Release]]
   */
  def updateRelease(
      tagName: String,
      description: String
  )(implicit ctx: GitLabContext, client: GitLabClient): GitLabIO[Release] =
    client.putNoBody(
      TagsUrl
        .append(tagName)
        .append("release")
        .withQueryParams(Description -> description)
    )

}
