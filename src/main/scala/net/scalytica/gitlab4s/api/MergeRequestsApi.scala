package net.scalytica.gitlab4s.api

import java.time.ZonedDateTime

import cats.effect.IO
import io.circe._
import io.circe.generic.extras.auto._
import net.scalytica.gitlab4s._
import net.scalytica.gitlab4s.api.MergeRequestsApi.CreateMergeRequestBody
import net.scalytica.gitlab4s.api.QueryParams._
import net.scalytica.gitlab4s.client.{GitLabClient, GitLabContext, GitLabUrl}
import net.scalytica.gitlab4s.models.commits.{Commit, CommitSha}
import net.scalytica.gitlab4s.models.issues.Issue
import net.scalytica.gitlab4s.models.mergerequests.AcceptResults.AcceptResult
import net.scalytica.gitlab4s.models.mergerequests.{
  MergeRequest,
  MergeRequestDiff,
  MergeRequestTodo,
  MergeState
}
import net.scalytica.gitlab4s.models.{
  GitLabUser,
  TimeEstimate,
  Scope => MergeRequestScope
}

trait MergeRequestsApi extends ApiBase {

  def MergeRequestsUrl(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("merge_requests")

  def ProjectMergeRequestsUrl(
      id: String
  )(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("projects").append(id).append("merge_requests")

  def GroupMergeRequestsUrl(
      id: String
  )(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("groups").append(id).append("merge_requests")

  implicit val mrSeqDecoder: Decoder[Seq[MergeRequest]] =
    Decoder.decodeSeq[MergeRequest]
  implicit val mrOptDecoder: Decoder[Option[MergeRequest]] =
    Decoder.decodeOption[MergeRequest]
  /*

   */
  /**
   * Get all merge requests the authenticated user has access to. By
   * default it returns only merge requests created by the current user. To
   * get all merge requests, use parameter scope=all.
   * The state parameter can be used to get only merge requests with a
   * given state (opened, closed, locked, or merged) or all of them (all). It
   * should be noted that when searching by locked it will mostly return no
   * results as it is a short-lived, transitional state. The pagination
   * parameters page and per_page can be used to restrict the list of merge
   * requests.
   *
   * @param state Return all merge requests or just those that are opened,
   *              closed, locked, or merged.
   * @param orderBy Return requests ordered by created_at or updated_at fields.
   *                 Default is created_at.
   * @param sort Return requests sorted in asc or desc order. Default is desc.
   * @param milestone Return merge requests for a specific milestone.
   * @param view If simple, returns the iid, URL, title, description, and basic
   *             state of merge request.
   * @param labels Return merge requests matching a comma separated list of
   *               labels.
   * @param createdAfter Return merge requests created on or after the given
   *                      time.
   * @param createdBefore Return merge requests created on or before the given
   *                       time.
   * @param updatedAfter Return merge requests updated on or after the given
   *                      time.
   * @param updatedBefore Return merge requests updated on or before the given
   *                       time.
   * @param scope Return merge requests for the given scope: created_by_me,
   *              assigned_to_me or all. Defaults to created_by_me For versions
   *              before 11.0, use the now deprecated created-by-me or
   *              assigned-to-me scopes instead.
   * @param authorId Returns merge requests created by the given user id.
   *                  Combine with scope=all or scope=assigned_to_me.
   * @param assigneeId Returns merge requests assigned to the given user id.
   * @param myReactionEmoji Return merge requests reacted by the authenticated
   *                          user by the given emoji.
   * @param sourceBranch Return merge requests with the given source branch.
   * @param targetBranch Return merge requests with the given target branch.
   * @param search Search merge requests against their title and description.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequest]]s.
   */
  def mergeRequests(
      state: Option[MergeState] = None,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      milestone: Option[String] = None,
      view: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      scope: Option[MergeRequestScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      sourceBranch: Option[String] = None,
      targetBranch: Option[String] = None,
      search: Option[String] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    val params = Seq.newBuilder[(String, String)]
    state.foreach(s => params += State                     -> s.value)
    orderBy.foreach(o => params += OrderBy                 -> o)
    sort.foreach(s => params += Sort                       -> s)
    milestone.foreach(m => params += Milestone             -> m)
    view.foreach(v => params += View                       -> v)
    createdAfter.foreach(c => params += CreatedAfter       -> c.toIso8601)
    createdBefore.foreach(c => params += CreatedBefore     -> c.toIso8601)
    updatedAfter.foreach(u => params += UpdatedAfter       -> u.toIso8601)
    updatedBefore.foreach(u => params += UpdatedBefore     -> u.toIso8601)
    scope.foreach(s => params += Scope                     -> s.value)
    authorId.foreach(a => params += AuthorId               -> a.toString)
    assigneeId.foreach(a => params += AssigneeId           -> a.toString)
    myReactionEmoji.foreach(m => params += MyReactionEmoji -> m)
    sourceBranch.foreach(s => params += SourceBranch       -> s)
    targetBranch.foreach(s => params += TargetBranch       -> s)
    search.foreach(s => params += Search                   -> s)
    if (labels.nonEmpty) params += Labels -> labels.mkString(",")

    client.paged(
      gitlabUrl = MergeRequestsUrl.withQueryParams(params.result(): _*),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get all merge requests for this project. The state parameter can be used to
   * get only merge requests with a given state (opened, closed, locked, or
   * merged) or all of them (all). The pagination parameters page and per_page
   * can be used to restrict the list of merge requests.
   *
   * @param projectId The ID of a project.
   * @param iids Return the request having the given iid.
   * @param state Return all merge requests or just those that are opened,
   *              closed, locked, or merged.
   * @param orderBy Return requests ordered by created_at or updated_at fields.
   *                Default is created_at.
   * @param sort Return requests sorted in asc or desc order. Default is desc.
   * @param milestone Return merge requests for a specific milestone.
   * @param view If simple, returns the iid, URL, title, description, and basic
   *             state of merge request.
   * @param labels Return merge requests matching a comma separated list of
   *               labels.
   * @param createdAfter Return merge requests created on or after the given
   *                     time.
   * @param createdBefore Return merge requests created on or before the given
   *                      time.
   * @param updatedAfter Return merge requests updated on or after the given
   *                     time.
   * @param updatedBefore Return merge requests updated on or before the given
   *                      time.
   * @param scope Return merge requests for the given scope: created_by_me,
   *              assigned_to_me or all. For versions before 11.0, use the now
   *              deprecated created-by-me or assigned-to-me scopes instead.
   * @param authorId Returns merge requests created by the given user id.
   * @param assigneeId Returns merge requests assigned to the given user id.
   * @param myReactionEmoji Return merge requests reacted by the authenticated
   *                        user by the given emoji.
   * @param sourceBranch Return merge requests with the given source branch.
   * @param targetBranch Return merge requests with the given target branch.
   * @param search Search merge requests against their title and description.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequest]]s.
   */
  def mergeRequestsForProject(
      projectId: Int,
      iids: Seq[Int],
      state: Option[MergeState] = None,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      milestone: Option[String] = None,
      view: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      scope: Option[MergeRequestScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      sourceBranch: Option[String] = None,
      targetBranch: Option[String] = None,
      search: Option[String] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    val params = Seq.newBuilder[(String, String)]
    state.foreach(s => params += State                     -> s.value)
    orderBy.foreach(o => params += OrderBy                 -> o)
    sort.foreach(s => params += Sort                       -> s)
    milestone.foreach(m => params += Milestone             -> m)
    view.foreach(v => params += View                       -> v)
    createdAfter.foreach(c => params += CreatedAfter       -> c.toIso8601)
    createdBefore.foreach(c => params += CreatedBefore     -> c.toIso8601)
    updatedAfter.foreach(u => params += UpdatedAfter       -> u.toIso8601)
    updatedBefore.foreach(u => params += UpdatedBefore     -> u.toIso8601)
    scope.foreach(s => params += Scope                     -> s.value)
    authorId.foreach(a => params += AuthorId               -> a.toString)
    assigneeId.foreach(a => params += AssigneeId           -> a.toString)
    myReactionEmoji.foreach(m => params += MyReactionEmoji -> m)
    sourceBranch.foreach(s => params += SourceBranch       -> s)
    targetBranch.foreach(s => params += TargetBranch       -> s)
    search.foreach(s => params += Search                   -> s)
    if (iids.nonEmpty) params += IidArray -> iids.mkString(",")
    if (labels.nonEmpty) params += Labels -> labels.mkString(",")

    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId.toString)
        .withQueryParams(params.result(): _*),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get all merge requests for this group and its subgroups. The state
   * parameter can be used to get only merge requests with a given state
   * (opened, closed, locked, or merged) or all of them (all). The pagination
   * parameters page and per_page can be used to restrict the list of merge
   * requests.
   *
   * @param groupId represents the ID of the group which contains the project
   *                where the MR resides.
   * @param state Return all merge requests or just those that are opened,
   *              closed, locked, or merged.
   * @param orderBy Return requests ordered by created_at or updated_at fields.
   *                Default is created_at.
   * @param sort Return requests sorted in asc or desc order. Default is desc.
   * @param milestone Return merge requests for a specific milestone.
   * @param view If simple, returns the iid, URL, title, description, and basic
   *             state of merge request.
   * @param labels Return merge requests matching a comma separated list of
   *               labels.
   * @param createdAfter Return merge requests created on or after the given
   *                     time.
   * @param createdBefore Return merge requests created on or before the given
   *                      time.
   * @param updatedAfter Return merge requests updated on or after the given
   *                     time.
   * @param updatedBefore Return merge requests updated on or before the given
   *                      time.
   * @param scope Return merge requests for the given scope: created_by_me,
   *              assigned_to_me or all. For versions before 11.0, use the now
   *              deprecated created-by-me or assigned-to-me scopes instead.
   * @param authorId Returns merge requests created by the given user id.
   * @param assigneeId Returns merge requests assigned to the given user id.
   * @param myReactionEmoji Return merge requests reacted by the authenticated
   *                        user by the given emoji.
   * @param sourceBranch Return merge requests with the given source branch.
   * @param targetBranch Return merge requests with the given target branch.
   * @param search Search merge requests against their title and description.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequest]]s.
   */
  def mergeRequestsForGroup(
      groupId: Int,
      state: Option[MergeState] = None,
      orderBy: Option[String] = None,
      sort: Option[String] = None,
      milestone: Option[String] = None,
      view: Option[String] = None,
      labels: Seq[String] = Seq.empty,
      createdAfter: Option[ZonedDateTime] = None,
      createdBefore: Option[ZonedDateTime] = None,
      updatedAfter: Option[ZonedDateTime] = None,
      updatedBefore: Option[ZonedDateTime] = None,
      scope: Option[MergeRequestScope] = None,
      authorId: Option[Int] = None,
      assigneeId: Option[Int] = None,
      myReactionEmoji: Option[String] = None,
      sourceBranch: Option[String] = None,
      targetBranch: Option[String] = None,
      search: Option[String] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    val params = Seq.newBuilder[(String, String)]
    state.foreach(s => params += State                     -> s.value)
    orderBy.foreach(o => params += OrderBy                 -> o)
    sort.foreach(s => params += Sort                       -> s)
    milestone.foreach(m => params += Milestone             -> m)
    view.foreach(v => params += View                       -> v)
    createdAfter.foreach(c => params += CreatedAfter       -> c.toIso8601)
    createdBefore.foreach(c => params += CreatedBefore     -> c.toIso8601)
    updatedAfter.foreach(u => params += UpdatedAfter       -> u.toIso8601)
    updatedBefore.foreach(u => params += UpdatedBefore     -> u.toIso8601)
    scope.foreach(s => params += Scope                     -> s.value)
    authorId.foreach(a => params += AuthorId               -> a.toString)
    assigneeId.foreach(a => params += AssigneeId           -> a.toString)
    myReactionEmoji.foreach(m => params += MyReactionEmoji -> m)
    sourceBranch.foreach(s => params += SourceBranch       -> s)
    targetBranch.foreach(s => params += TargetBranch       -> s)
    search.foreach(s => params += Search                   -> s)
    if (labels.nonEmpty) params += Labels -> labels.mkString(",")

    client.paged(
      gitlabUrl = GroupMergeRequestsUrl(groupId.toString)
        .withQueryParams(params.result(): _*),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Shows information about a single merge request.
   *
   * <b>Note:</b> the changes_count value in the response is a string, not an
   * integer. This is because when an MR has too many changes to display and
   * store, it will be capped at 1,000. In that case, the API will return the
   * string "1000+" for the changes count.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param renderHtml If true response includes rendered HTML for title and
   *                   description.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with an optional [[MergeRequest]].
   */
  def mergeRequest(
      projectId: String,
      mergeRequestIid: Int,
      renderHtml: Boolean = false
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[MergeRequest]] = {
    client.get(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .withQueryParams(RenderHtml -> renderHtml.toString)
    )
  }

  /**
   * Get a list of merge request participants.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[GitLabUser]]s.
   */
  def participants(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[GitLabUser]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("participants"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get a list of merge request commits.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Commit]]s.
   */
  def commits(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Commit]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("commits"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Shows the merge request and information about the changes made, including
   * its files.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequest]]s.
   */
  def changes(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("changes"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get a list of merge request pipelines.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequest]]s.
   */
  def pipelines(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("pipelines"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Creates a new merge request.
   *
   * If {{{approvalsBeforeMerge}}} is not provided, it inherits the value from
   * the target project. If it is provided, then the following conditions must
   * hold in order for it to take effect:
   *
   * <ol>
   *   <li>
   *     The target project's {{{approvalsBeforeMerge}}} must be greater than
   *     zero. (A value of zero disables approvals for that project.)
   *   </li>
   *   <li>
   *     The provided value of {{{approvalsBeforeMerge}}} must be greater than
   *     the target project's {{{approvalsBeforeMerge}}}.
   *   </li>
   * </ol>
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param sourceBranch The source branch.
   * @param targetBranch The target branch.
   * @param title Title of MR.
   * @param assigneeId Assignee user ID.
   * @param description Description of MR.
   * @param targetProjectId The target project (numeric id).
   * @param labels Labels for MR as a comma-separated list.
   * @param milestoneId The ID of a milestone.
   * @param removeSourceBranch Flag indicating if a merge request should remove
   *                           the source branch when merging.
   * @param allowCollaboration Allow commits from members who can merge to the
   *                           target branch.
   * @param allowMaintainerToPush Deprecated, see allow_collaboration.
   * @param squash Squash commits into a single commit when merging.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the created [[MergeRequest]].
   */
  def create(
      projectId: String,
      sourceBranch: String,
      targetBranch: String,
      title: String,
      assigneeId: Option[Int] = None,
      description: Option[String] = None,
      targetProjectId: Option[Int] = None,
      labels: Seq[String] = Seq.empty,
      milestoneId: Option[Int] = None,
      removeSourceBranch: Option[Boolean] = None,
      allowCollaboration: Option[Boolean] = None,
      allowMaintainerToPush: Option[Boolean] = None,
      squash: Option[Boolean] = None
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[MergeRequest] = {
    val body = CreateMergeRequestBody(
      sourceBranch = sourceBranch,
      targetBranch = targetBranch,
      title = title,
      assigneeId = assigneeId,
      description = description,
      targetProjectId = targetProjectId,
      labels = labels,
      milestoneId = milestoneId,
      removeSourceBranch = removeSourceBranch,
      allowCollaboration = allowCollaboration,
      allowMaintainerToPush = allowMaintainerToPush,
      squash = squash
    )

    client.post(
      gitlabUrl = ProjectMergeRequestsUrl(projectId),
      data = body
    )
  }

  /**
   * Updates an existing merge request. You can change the target branch, title,
   * or even close the MR.
   *
   * Must include at least one optional attribute from below.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param mergeRequestIid The ID of a merge request.
   * @param targetBranch The target branch.
   * @param title Title of MR.
   * @param assigneeId The ID of the user to assign the merge request to. Set to
   *                   0 or provide an empty value to unset all assignees.
   * @param milestoneId The ID of a milestone to assign the merge request to.
   *                    Set to 0 or provide an empty value to unset a milestone.
   * @param labels Comma-separated label names for a merge request. Set to an
   *               empty string to unassign all labels.
   * @param description Description of MR
   * @param stateEvent New state (close/reopen).
   * @param removeSourceBranch Flag indicating if a merge request should remove
   *                           the source branch when merging.
   * @param squash Squash commits into a single commit when merging.
   * @param discussionLocked Flag indicating if the merge request's discussion
   *                         is locked. If the discussion is locked only project
   *                         members can add, edit or resolve comments.
   * @param allowCollaboration Allow commits from members who can merge to the
   *                           target branch.
   * @param allowMaintainerToPush Deprecated, see allow_collaboration.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the updated [[MergeRequest]].
   */
  def update(
      projectId: String,
      mergeRequestIid: Int,
      targetBranch: Option[String],
      title: Option[String],
      assigneeId: Option[Int],
      milestoneId: Option[Int],
      labels: Seq[String] = Seq.empty,
      description: Option[String],
      stateEvent: Option[String],
      removeSourceBranch: Option[Boolean],
      squash: Option[Boolean],
      discussionLocked: Option[Boolean],
      allowCollaboration: Option[Boolean],
      allowMaintainerToPush: Option[Boolean]
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[MergeRequest]] = {
    val params = Seq.newBuilder[(String, String)]
    targetBranch.foreach(t => params += TargetBranch             -> t)
    title.foreach(t => params += Title                           -> t)
    assigneeId.foreach(a => params += AssigneeId                 -> a.toString)
    milestoneId.foreach(m => params += MilestoneId               -> m.toString)
    description.foreach(d => params += Description               -> d)
    stateEvent.foreach(s => params += StateEvent                 -> s)
    removeSourceBranch.foreach(r => params += RemoveSourceBranch -> r.toString)
    squash.foreach(s => params += Squash                         -> s.toString)
    discussionLocked.foreach(d => params += DiscussionLocked     -> d.toString)
    allowCollaboration.foreach(a => params += AllowCollaboration -> a.toString)
    allowMaintainerToPush.foreach { a =>
      params += AllowMaintainerToPush -> a.toString
    }
    if (labels.nonEmpty) params += Labels -> labels.mkString(",")

    client.putNoBody(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .withQueryParams(params.result(): _*)
    )
  }

  /**
   * Only for admins and project owners. Soft deletes the given merge request.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a Unit result.
   */
  def delete(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Unit] = {
    // DELETE /projects/:id/merge_requests/:merge_request_iid
    client.delete(ProjectMergeRequestsUrl(projectId).append(mergeRequestIid))
  }

  /**
   * Merge changes submitted with MR using this API.
   *
   * @param projectId The ID or URL-encoded path of the project owned by the
   *                  authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param mergeCommitMessage Custom merge commit message.
   * @param shouldRemoveSourceBranch If true removes the source branch.
   * @param mergeWhenPipelineSucceeds If true the MR is merged when the pipeline
   *                                  succeeds.
   * @param sha If present, then this SHA must match the HEAD of the source
   *            branch, otherwise the merge will fail.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the accepted [[MergeRequest]].
   */
  def accept(
      projectId: String,
      mergeRequestIid: Int,
      mergeCommitMessage: Option[String] = None,
      shouldRemoveSourceBranch: Option[Boolean] = None,
      mergeWhenPipelineSucceeds: Option[Boolean] = None,
      sha: Option[CommitSha] = None
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Either[AcceptResult, MergeRequest]] = {
    // TODO think about the return type
    // PUT /projects/:id/merge_requests/:merge_request_iid/merge
    ???
  }

  /**
   * Cancel Merge When Pipeline Succeeds
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the [[MergeRequest]] that was cancelled.
   */
  def cancelOnSuccessfulPipeline(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[MergeRequest] = {
    client.putNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("cancel_merge_when_pipeline_succeeds")
    )
  }

  /**
   * Get all the issues that would be closed by merging the provided merge
   * request.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[Issue]]s.
   */
  def closesIssues(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Issue]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("closes_issues"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Subscribes the authenticated user to a merge request to receive
   * notification. If the user is already subscribed to the merge request, the
   * status code 304 is returned.
   *
   * TODO: handle 304 situation
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the [[MergeRequest]] being subscribed to.
   */
  def subscribe(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[MergeRequest] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("subscribe")
    )
  }

  /**
   * Unsubscribes the authenticated user from a merge request to not receive
   * notifications from that merge request. If the user is not subscribed to the
   * merge request, the status code 304 is returned.
   *
   * TODO: handle 304 situation
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the [[MergeRequest]] being unsubscribed to.
   */
  def unsubscribe(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[MergeRequest] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("unsubscribe")
    )
  }

  /**
   * Manually creates a [[MergeRequestTodo]] for the current user on a merge
   * request. If one already exists for the user on that merge request, status
   * code 304 is returned.
   *
   * TODO: handle 304 situation
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the created [[MergeRequestTodo]].
   */
  def createTodo(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[MergeRequestTodo] = {
    // POST /projects/:id/merge_requests/:merge_request_iid/todo
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId).append(mergeRequestIid).append("todo")
    )
  }

  /**
   * Get a list of merge request diff versions.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param page The page number to retrieve.
   * @param perPage Number of records per page.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with a collection of [[MergeRequestDiff]]s.
   */
  def diffVersions(
      projectId: String,
      mergeRequestIid: Int,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequestDiff]] = {
    client.paged(
      gitlabUrl = ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("versions"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get a single merge request diff version.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param versionId The ID of the merge request diff version.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with an optional [[MergeRequestDiff]].
   */
  def diffVersion(
      projectId: String,
      mergeRequestIid: Int,
      versionId: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): IO[Option[MergeRequestDiff]] = {
    client.get(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("versions")
        .append(versionId)
    )
  }

  /**
   * Sets an estimated time of work for this merge request.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param duration The duration in human format. e.g: 3h30m.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the set [[TimeEstimate]].
   */
  def setTimeEstimate(
      projectId: String,
      mergeRequestIid: Int,
      duration: String
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("time_estimate")
        .withQueryParams(TimeDuration -> duration)
    )
  }

  /**
   * Resets the estimated time for this merge request to 0 seconds.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the affected [[TimeEstimate]].
   */
  def resetTimeEstimate(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("reset_time_estimate")
    )
  }

  /**
   * Adds spent time for this merge request.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param duration The duration in human format. e.g: 3h30m.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the set [[TimeEstimate]].
   */
  def addTimeSpent(
      projectId: String,
      mergeRequestIid: Int,
      duration: String
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("add_spent_time")
        .withQueryParams(TimeDuration -> duration)
    )
  }

  /**
   * Resets the total spent time for this merge request to 0 seconds.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with the affected [[TimeEstimate]].
   */
  def resetTimeSpent(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[TimeEstimate] = {
    client.postNoBody(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("reset_spent_time")
    )
  }

  /**
   * Get time tracking stats.
   *
   * @param projectId  The ID or URL-encoded path of the project owned by the
   *                   authenticated user.
   * @param mergeRequestIid The internal ID of the merge request.
   * @param ctx The context to use for this API call.
   * @param client The [[GitLabClient]] to use when calling this function.
   * @return An IO monad with an optional [[TimeEstimate]].
   */
  def timeStats(
      projectId: String,
      mergeRequestIid: Int
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[TimeEstimate]] = {
    client.get(
      ProjectMergeRequestsUrl(projectId)
        .append(mergeRequestIid)
        .append("time_stats")
    )
  }
}

object MergeRequestsApi {

  private[gitlab4s] case class CreateMergeRequestBody(
      sourceBranch: String,
      targetBranch: String,
      title: String,
      assigneeId: Option[Int],
      description: Option[String],
      targetProjectId: Option[Int],
      labels: Seq[String],
      milestoneId: Option[Int],
      removeSourceBranch: Option[Boolean],
      allowCollaboration: Option[Boolean],
      allowMaintainerToPush: Option[Boolean],
      squash: Option[Boolean]
  )

}
