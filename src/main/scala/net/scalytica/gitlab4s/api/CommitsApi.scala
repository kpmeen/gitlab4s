package net.scalytica.gitlab4s.api

import java.time.ZonedDateTime

import io.circe._
import io.circe.generic.extras.auto._
import net.scalytica.gitlab4s._
import net.scalytica.gitlab4s.api.QueryParams._
import net.scalytica.gitlab4s.client.{GitLabClient, GitLabContext, GitLabUrl}
import net.scalytica.gitlab4s.models.commits.{CommitReference, _}
import net.scalytica.gitlab4s.models.mergerequests.MergeRequest
import net.scalytica.gitlab4s.models.pipelines.{Pipeline, PipelineStatus}
import net.scalytica.gitlab4s.models.{Comment, LineType}

trait CommitsApi extends ApiBase {

  def CommitsUrl(implicit ctx: GitLabContext): GitLabUrl =
    BaseProjUrl.append("/repository/commits")

  implicit val commitSeqDecorder: Decoder[Seq[Commit]] =
    Decoder.decodeSeq[Commit]
  implicit val commitOptDecoder: Decoder[Option[Commit]] =
    Decoder.decodeOption[Commit]

  /**
   * Get a all repository commits in a project.
   *
   * @param refName The name of a repository branch or tag or if not given the
   *                default branch
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[Commit]]s
   */
  def all(refName: String)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Commit]] = client.all(
    CommitsUrl
      .withPerPage(MaxResultsPerPage)
      .withQueryParams(
        RefName -> refName,
        All     -> "true"
      )
  )

  /**
   * Get a list of repository commits in a project.
   *
   * @param refName   The name of a repository branch or tag or if not given the
   *                  default branch
   * @param since     Only commits after or on this date will be returned in
   *                  ISO 8601 format YYYY-MM-DDTHH:MM:SSZ
   * @param until     Only commits before or on this date will be returned in
   *                  ISO 8601 format YYYY-MM-DDTHH:MM:SSZ
   * @param path      The file path
   * @param withStats Stats about each commit will be added to the response
   * @param page      The page number to retrieve
   * @param perPage   Number of commits per page
   * @param ctx       The context to use for this API call
   * @param client    The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[Commit]]s
   */
  def commits(
      refName: String,
      since: Option[ZonedDateTime] = None,
      until: Option[ZonedDateTime] = None,
      path: Option[String] = None,
      withStats: Boolean = false,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Commit]] = {
    val params = Seq.newBuilder[(String, String)]
    params += RefName   -> refName
    params += WithStats -> s"$withStats"
    since.foreach(s => params += Since -> s.toIso8601)
    until.foreach(u => params += Until -> u.toIso8601)
    path.foreach(p => params += Path   -> p)

    client.paged[Commit](
      gitlabUrl = CommitsUrl.withQueryParams(params.result(): _*),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get a specific commit identified by the commit hash or name of a branch or
   * tag.
   *
   * @param sha    The [[CommitSha]] to retrieve
   * @param stats  Include commit stats. Default is true
   * @param ctx    The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad with an Optional of [[Commit]]
   */
  def commit(sha: CommitSha, stats: Boolean = true)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[Commit]] = {
    client.get(CommitsUrl.append(sha.value).withQueryParams(Stats -> s"$stats"))
  }

  /**
   * Get all references (from branches or tags) a commit is pushed to. The
   * pagination parameters {{{page}}} and {{{perPage}}} can be used to restrict
   * the list of references.
   *
   * @param sha     The [[CommitSha]] to retrieve
   * @param tpe     The scope of commits. Possible values branch, tag, all.
   * @param page    The page number to retrieve
   * @param perPage Number of commits per page
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[CommitReference]]s
   */
  def references(
      sha: CommitSha,
      tpe: CommitReference.ReferenceType = CommitReference.All,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[CommitReference]] = {
    val url = CommitsUrl
      .append(sha.value)
      .append("/refs")
      .withQueryParams(Type -> tpe.value)

    client.paged(
      gitlabUrl = url,
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get the diff of a commit in a project.
   *
   * @param sha     The [[CommitSha]] to retrieve
   * @param page    The page number to retrieve
   * @param perPage Number of commits per page
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[CommitDiff]]s
   */
  def diff(
      sha: CommitSha,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[CommitDiff]] =
    client.paged(
      gitlabUrl = CommitsUrl.append(sha.value).append("/diff"),
      page = page,
      perPage = perPage
    )

  /**
   * Get the comments of a commit in a project.
   *
   * @param sha     The [[CommitSha]] to retrieve
   * @param page    The page number to retrieve
   * @param perPage Number of commits per page
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[Comment]]s
   */
  def comments(
      sha: CommitSha,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[Comment]] =
    client.paged(
      gitlabUrl = CommitsUrl.append(sha.value).append("/comments"),
      page = page,
      perPage = perPage
    )

  /**
   * List all statuses of a commit in a project.
   *
   * @param sha    The [[CommitSha]] to retrieve
   * @param ctx    The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[CommitStatus]]s
   */
  def allStatuses(sha: CommitSha)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[CommitStatus]] =
    client.all(CommitsUrl.append(sha.value).append("/statuses"))

  /**
   * List the statuses of a commit in a project. The pagination parameters
   * {{{page}}} and {{{perPage}}} can be used to restrict the list of
   * references.
   *
   * @param sha     The [[CommitSha]] to retrieve
   * @param ref     The name of a repository branch or tag or, if not given,
   *                the default branch
   * @param stage   Filter by build stage, e.g., test
   * @param name    Filter by job name, e.g., bundler:audit
   * @param page    The page number to retrieve
   * @param perPage Number of commits per page
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[CommitStatus]]s
   */
  def statuses(
      sha: CommitSha,
      ref: Option[String] = None,
      stage: Option[String] = None,
      name: Option[String] = None,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[CommitStatus]] = {
    val params = Seq.newBuilder[(String, String)]
    ref.foreach(r => params += Ref     -> r)
    stage.foreach(s => params += Stage -> s)
    name.foreach(n => params += Name   -> n)

    client.paged(
      gitlabUrl = CommitsUrl.append(sha.value).append("/statuses"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Get a list of Merge Requests related to the specified commit.
   *
   * @param sha     The [[CommitSha]] to retrieve
   * @param page    The page number to retrieve
   * @param perPage Number of commits per page
   * @param ctx     The context to use for this API call
   * @param client  The [[GitLabClient]] to use when calling this function
   * @return An IO monad with a collection of [[MergeRequest]]s
   */
  def mergeRequests(
      sha: CommitSha,
      page: Int = 1,
      perPage: Int = GitLabClient.DefaultPerPage
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Seq[MergeRequest]] = {
    client.paged(
      gitlabUrl = CommitsUrl.append(sha.value).append("/merge_requests"),
      page = page,
      perPage = perPage
    )
  }

  /**
   * Create a commit.
   *
   * @param ac     The commit to add
   * @param ctx    The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the added [[Commit]]
   */
  def create(ac: AddCommit)(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Commit] = client.post[AddCommit, Commit](CommitsUrl, ac)

  /**
   * Cherry picks a commit to a given branch.
   *
   * @param sha    The [[CommitSha]] to cherry pick
   * @param branch The branch to apply the commit to
   * @param ctx    The context to use for this API call
   * @param client The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the cherry picked [[Commit]]
   */
  def cherryPick(
      sha: CommitSha,
      branch: String
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Commit] = {
    client.postForm(
      gitlabUrl = CommitsUrl.append(sha.value).append("/cherry_pick"),
      formData = Map(
        Sha    -> sha.value,
        Branch -> branch
      )
    )
  }

  /**
   * Adds a comment to a commit.
   *
   * In order to post a comment in a particular line of a particular file, you
   * must specify the full commit SHA, the path, the line and line_type should
   * be new.
   *
   * The comment will be added at the end of the last commit if at least one of
   * the cases below is valid:
   *
   * - the sha is instead a branch or a tag and the line or path are invalid
   * - the line number is invalid (does not exist)
   * - the path is invalid (does not exist)
   *
   * In any of the above cases, the response of line, line_type and path is
   * set to null.
   *
   * @param sha      The [[CommitSha]] to comment on
   * @param note     The text of the comment
   * @param path     The file path relative to the repository
   * @param line     The line number where the comment should be placed
   * @param lineType The [[LineType]] for the comment. New or old line.
   * @param ctx      The context to use for this API call
   * @param client   The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the new [[Comment]]
   */
  def addComment(
      sha: CommitSha,
      note: String,
      path: Option[String],
      line: Option[Int],
      lineType: Option[LineType]
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Comment] = {
    val formParams = Map.newBuilder[String, String]
    formParams += "note" -> note
    path.foreach(p => formParams += Path         -> p)
    line.foreach(l => formParams += Line         -> s"$l")
    lineType.foreach(l => formParams += LineType -> l.value)

    client.postForm(
      gitlabUrl = CommitsUrl.append(sha.value).append("/comments"),
      formData = formParams.result()
    )
  }

  /**
   * Adds or updates a build status of a commit.
   *
   * @param sha         The commit SHA to add build status to
   * @param state       The pipeline status.
   * @param ref         The ref (branch or tag) to which the status refers
   * @param name        The label to differentiate this status from the status
   *                    of other systems.
   * @param targetUrl   The target URL to associate with this status
   * @param description The short description of the status
   * @param coverage    The total code coverage
   * @param ctx         The context to use for this API call
   * @param client      The [[GitLabClient]] to use when calling this function
   * @return An IO monad containing the new build status
   */
  def addBuildStatus(
      sha: CommitSha,
      state: PipelineStatus,
      ref: Option[String],
      name: Option[String], // name or context
      targetUrl: Option[String],
      description: Option[String],
      coverage: Option[Float]
  )(
      implicit ctx: GitLabContext,
      client: GitLabClient
  ): GitLabIO[Option[Pipeline]] = {
    val formParams = Map.newBuilder[String, String]
    formParams += State -> state.value
    ref.foreach(r => formParams += Ref                 -> r)
    name.foreach(n => formParams += Name               -> n)
    targetUrl.foreach(t => formParams += TargetUrl     -> t)
    description.foreach(d => formParams += Description -> d)
    coverage.foreach(c => formParams += Coverage       -> s"$c")

    client.postForm(
      gitlabUrl = BaseProjUrl.append("statuses").append(sha.value),
      formData = formParams.result()
    )
  }
}
