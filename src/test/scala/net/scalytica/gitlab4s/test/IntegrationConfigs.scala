package net.scalytica.gitlab4s.test

import java.nio.file.{Path, Paths}

import net.scalytica.gitlab4s.client.{AccessToken, GitLabHost, ProjectUri}

trait IntegrationConfigs {

  private[this] val tokenKey = "GITLAB_API_TOKEN"

  val host = GitLabHost()

  val projectUri = ProjectUri("kpmeen", "clammyscan")

  val token = AccessToken(sys.props.get(tokenKey).getOrElse(sys.env(tokenKey)))

  val testDestPath: Path = Paths.get("target/TEST-CHANGELOG.md")

}
