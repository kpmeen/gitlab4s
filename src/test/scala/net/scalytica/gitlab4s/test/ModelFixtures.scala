package net.scalytica.gitlab4s.test

import java.security.MessageDigest
import java.time.ZonedDateTime

import net.scalytica.gitlab4s.models.commits.{Commit, CommitSha, CommitShortSha}
import net.scalytica.gitlab4s.models.tags.Tag
import net.scalytica.gitlab4s.models.{Commit, CommitShortRef}

// scalastyle:off magic.number
/**
 * Test fixtures for different data types
 */
trait ModelFixtures {

  val now: ZonedDateTime = ZonedDateTime.now()

  def sha1(s: String): String = {
    val hexChars = "0123456789abcdef".toCharArray

    val sha: Array[Byte] =
      MessageDigest.getInstance("SHA-1").digest(s.mkString.getBytes("UTF-8"))

    sha.foldLeft("") { (str, byte) =>
      str + hexChars((byte & 0xF0) >> 4) + hexChars(byte & 0x0F)
    }
  }

  def sha1Short(s: String): String = sha1(s).take(11)

  def commitRef: CommitSha = CommitSha(sha1("test-ref-1"))

  def commitShortRef: CommitShortSha = CommitShortSha(sha1Short("test-ref-1"))

  // scalastyle:off parameter.number
  def singleCommit(
      id: CommitSha = commitRef,
      shortId: CommitShortSha = commitShortRef,
      title: String = "test-commit-1",
      createdAt: ZonedDateTime = now,
      message: Option[String] = Some("Test commit message 1"),
      committerName: Option[String] = Some("Darth Vader"),
      committerEmail: Option[String] = Some("darth@empire.com"),
      committedDate: Option[ZonedDateTime] = Some(now),
      parentIds: Option[List[CommitSha]] = None
  ): Commit = Commit(
    id = id,
    shortId = shortId,
    title = title,
    createdAt = createdAt,
    message = message,
    committerName = committerName,
    committerEmail = committerEmail,
    committedDate = committedDate,
    authorName = None,
    authorEmail = None,
    authoredDate = None,
    parentIds = parentIds
  )
  // scalastyle:on parameter.number

  def commits(num: Int = 5): List[Commit] = (1 to num).toList.map { i =>
    val ts = now.minusHours(i.toLong)
    singleCommit(
      id = CommitSha(sha1(s"test-ref-$i")),
      shortId = CommitShortSha(sha1Short(s"test-ref-$i")),
      title = s"test-commit-$i",
      createdAt = ts,
      message = Some(s"Test commit message $i"),
      committedDate = Some(ts)
    )
  }

  def tag(
      name: String = "test-tag-1",
      target: CommitSha = CommitSha(sha1("test-tagref-1")),
      commit: Commit = singleCommit(),
      message: Option[String] = Some("Test Tag 1")
  ): Tag = Tag(
    name = name,
    target = target,
    commit = commit,
    message = message,
    release = None
  )

  def tags(commitsToTag: List[Commit]): List[Tag] =
    commitsToTag.zipWithIndex.map {
      case (commit, i) =>
        Tag(
          name = s"test-tag-${i + 1}",
          target = CommitSha(sha1(s"test-tagref-$i")),
          commit = commit,
          message = Some(s"Test Tag ${i + 1}")
        )
    }

  def tagsAndCommits(
      numTags: Int = 5,
      commitsPerTag: Int = 3
  ): (List[Tag], List[Commit]) = {
    val c = commits(commitsPerTag * numTags).sortBy(_.createdAt)
    val t = tags(c.grouped(commitsPerTag).toList.map(_.last))
    (t, c)
  }

}

// scalastyle:on magic.number
