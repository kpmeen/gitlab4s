package net.scalytica.gitlab4s.models

import java.time.ZonedDateTime

import cats.syntax.either._
import io.circe._
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.jackson._
import net.scalytica.gitlab4s.models.commits.{Commit, CommitSha, CommitShortSha}
import org.scalatest.{MustMatchers, WordSpec}

class CommitSpec extends WordSpec with MustMatchers {

  implicit val snakeCaseConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames

  implicit val decodeInstant: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { str =>
      Either
        .catchNonFatal(ZonedDateTime.parse(str))
        .leftMap(t => s"ZonedDateTime could not be parsed: ${t.getMessage}")
    }

  val json: String =
    """{
      |  "author_email": "vader@empire.org",
      |  "author_name": "Darth Vader",
      |  "authored_date": "2018-05-24T21:55:46.000Z",
      |  "committed_date": "2018-05-24T21:55:46.000Z",
      |  "committer_email": "vader@empire.org",
      |  "committer_name": "Darth Vader",
      |  "created_at": "2018-05-24T21:55:46.000Z",
      |  "id": "2e2b6b4c5e198dac2f3ea6cab9831c7b3e73fde1",
      |  "message": "Update .gitlab-ci.yml",
      |  "parent_ids": [
      |      "3effdd5ce7b017ec94f33c2c8c420711bb29b4db"
      |  ],
      |  "short_id": "2e2b6b4c",
      |  "title": "Update .gitlab-ci.yml"
      |}""".stripMargin

  val expectedName  = "Darth Vader"
  val expectedEmail = "vader@empire.org"
  val expectedDate: ZonedDateTime =
    ZonedDateTime.parse("2018-05-24T21:55:46.000Z")

  "A commit" should {

    "be successfully initialised from a JSON string" in {
      decode[Commit](json) match {
        case Right(c) =>
          c.title mustBe "Update .gitlab-ci.yml"
          c.shortId mustBe CommitShortSha("2e2b6b4c")
          c.id mustBe CommitSha("2e2b6b4c5e198dac2f3ea6cab9831c7b3e73fde1")
          c.message mustBe Some("Update .gitlab-ci.yml")
          c.parentIds mustBe Some(
            List(CommitSha("3effdd5ce7b017ec94f33c2c8c420711bb29b4db"))
          )
          c.createdAt mustBe expectedDate
          c.authorEmail mustBe Some(expectedEmail)
          c.authorName mustBe Some(expectedName)
          c.authoredDate mustBe Some(expectedDate)
          c.committerEmail mustBe Some(expectedEmail)
          c.committerName mustBe Some(expectedName)
          c.committedDate mustBe Some(expectedDate)

        case Left(err) =>
          fail(err.getMessage)
      }
    }

  }
}
