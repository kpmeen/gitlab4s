package net.scalytica.gitlab4s.models

import java.time.ZonedDateTime

import cats.syntax.either._
import io.circe._
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.jackson._
import net.scalytica.gitlab4s.models.commits.{CommitSha, CommitShortSha}
import net.scalytica.gitlab4s.models.tags.Tag
import org.scalatest.{MustMatchers, WordSpec}

class TagSpec extends WordSpec with MustMatchers {

  implicit val snakeCaseConfig: Configuration =
    Configuration.default.withSnakeCaseMemberNames

  implicit val decodeInstant: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { str =>
      Either
        .catchNonFatal(ZonedDateTime.parse(str))
        .leftMap(t => s"ZonedDateTime could not be parsed: ${t.getMessage}")
    }

  val json: String =
    """{
      |  "commit": {
      |      "author_email": "vader@empire.org",
      |      "author_name": "Darth Vader",
      |      "authored_date": "2017-08-08T08:53:39.000Z",
      |      "committed_date": "2017-08-08T08:53:39.000Z",
      |      "committer_email": "vader@empire.org",
      |      "committer_name": "Darth Vader",
      |      "created_at": "2017-08-08T08:53:39.000Z",
      |      "id": "eeda7b0de844634f9a9d8518bcebaf88f0225d46",
      |      "message": "Setting version to 0.1.0\n",
      |      "parent_ids": [
      |          "b07b128b5190873a2875e58410cde138b3a05573"
      |      ],
      |      "short_id": "eeda7b0d",
      |      "title": "Setting version to 0.1.0"
      |  },
      |  "message": "Releasing 0.1.0",
      |  "name": "v0.1.0",
      |  "release": null,
      |  "target": "8815e20d96468ff9e3dd75b6cd3d7d99265f0f8f"
      }""".stripMargin

  val expectedName  = "Darth Vader"
  val expectedEmail = "vader@empire.org"
  val expectedDate: ZonedDateTime =
    ZonedDateTime.parse("2017-08-08T08:53:39.000Z")

  "A Tag" should {

    "be successfully be initialised from a JSON string" in {
      decode[Tag](json) match {
        case Right(t) =>
          t.name mustBe "v0.1.0"
          t.target mustBe CommitSha("8815e20d96468ff9e3dd75b6cd3d7d99265f0f8f")
          t.release mustBe None
          t.message mustBe Some("Releasing 0.1.0")

          val c = t.commit
          c.title mustBe "Setting version to 0.1.0"
          c.shortId mustBe CommitShortSha("eeda7b0d")
          c.id mustBe CommitSha("eeda7b0de844634f9a9d8518bcebaf88f0225d46")
          c.message mustBe Some("Setting version to 0.1.0\n")
          c.parentIds mustBe Some(
            List(CommitSha("b07b128b5190873a2875e58410cde138b3a05573"))
          )
          c.createdAt mustBe expectedDate
          c.authorEmail mustBe Some(expectedEmail)
          c.authorName mustBe Some(expectedName)
          c.authoredDate mustBe Some(expectedDate)
          c.committerEmail mustBe Some(expectedEmail)
          c.committerName mustBe Some(expectedName)
          c.committedDate mustBe Some(expectedDate)

        case Left(err) =>
          fail(err.getMessage)
      }
    }

  }

}
