//package net.scalytica.gitlab4s.client
//
//import net.scalytica.gitlab4s.models.commits.Commit
//import net.scalytica.gitlab4s.models.issues.Issue
//import net.scalytica.gitlab4s.models.milestones.Milestone
//import net.scalytica.gitlab4s.models.tags.Tag
//import net.scalytica.gitlab4s.test.IntegrationConfigs
//import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpec}
//
//class GitLabClientSpec
//    extends WordSpec
//    with BeforeAndAfterAll
//    with MustMatchers
//    with IntegrationConfigs {
//
//  val client = GitLabClient(host, token, ProjectUri("kpmeen", "symbiotic"))
//
//  override def afterAll(): Unit = {
//    client.close.unsafeRunSync()
//    super.afterAll()
//  }
//
//  "The GitLab client" should {
//
//    "fetch a list of all commits from GitLab" in {
//      val res: Seq[Commit] = client.getCommits.unsafeRunSync()
//      res must have size 689
//      // TODO: Add more detailed assertions of response
//    }
//
//    "fetch a list of all tags from GitLab" in {
//      val res: Seq[Tag] = client.getTags.unsafeRunSync()
//      res must have size 18
//      // TODO: Add more detailed assertions of response
//    }
//
//    "fetch a list of all issues from GitLab" in {
//      val res: Seq[Issue] = client.getIssues.unsafeRunSync()
//      res must not be empty
//      // TODO: Add more detailed assertions of response
//    }
//
//    "fetch a list of all milestones from GitLab" in {
//      val res: Seq[Milestone] = client.getMilestones.unsafeRunSync()
//      res must not be empty
//      // TODO: Add more detailed assertions of response
//    }
//
//  }
//
//}
